<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking', function (Blueprint $table) {
            $table->increments('id');
            $table->string('voucherNo');
            $table->string('voucherHolder');
            $table->string('entryDate');
            $table->string('nameOne')->nullable();
            $table->string('nameTwo')->nullable();
            $table->string('address');
            $table->string('email');
            $table->string('referenceNo')->nullable();
            $table->string('receipt')->nullable();
            $table->string('contactNo');
            $table->string('selectedOffer');
            $table->string('status')->default('NEW');
            $table->string('statusNote')->nullable();
            $table->string('paymentType');
            $table->string('bookingDate');
            $table->string('dateOne');
            $table->string('dateTwo')->nullable();
            $table->string('dateThree')->nullable();
            $table->string('chosenOutlet');
            $table->string('confirmedOutlet')->nullable();
            $table->string('dueDate');
            $table->string('confirmedDate')->nullable();
            $table->string('confirmedLocation')->nullable();
            $table->decimal('paymentAmount', 65, 2);
            $table->text('remarks')->nullable();
            $table->integer('additionalNights')->nullable();
            $table->integer('additionalRooms')->nullable();
            $table->string('hotelUpgrade')->default('false')->nullable();
            $table->string('roomUpgrade')->default('false')->nullable();
            $table->text('specialRequest')->nullable();
            $table->string('emailSentDate')->nullable();
            $table->timestampsTz();
            $table->string('last_edited_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking');
    }
}
