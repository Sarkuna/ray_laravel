<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project', function (Blueprint $table) {
            $table->increments('id');
            $table->string('clientName');
            $table->string('projectName');
            $table->string('projectPrefix');
            $table->string('title')->nullable();
            $table->text('greetingNote');
            $table->string('greetingBanner')->nullable();
            $table->string('clientLogo')->nullable();
            $table->string('expiryDate');
            $table->string('offers');
            $table->timestampsTz();
            $table->string('created_by');
            $table->string('last_edited_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project');
    }
}
