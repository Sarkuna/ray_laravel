<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer', function (Blueprint $table) {
            $table->increments('id');
            $table->string('referenceName');
            $table->string('offerName');
            $table->string('status')->default('active');
            $table->text('description');
            $table->string('termsConditions');
            $table->string('thumbnail')->nullable();
            $table->string('banner')->nullable();
            $table->string('bookingForm');
            $table->string('payment')->default('false');
            $table->string('bookingRate');
            $table->string('locations');
            $table->string('accommodations')->nullable();
            $table->string('groupType')->nullable();
            $table->string('expiryDate');
            $table->string('created_at')->nullable();
            $table->string('updated_at')->nullable();
            $table->string('created_by');
            $table->string('last_edited_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer');
    }
}