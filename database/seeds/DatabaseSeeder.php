<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Test',
            'status' => 'active',
            'email' => 'admin77@gmail.com',
            'password' => Hash::make('07070707'),
            'created_by' => 'Database Seed',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('booking')->insert([
            'voucherNo' => 'TTL 0001',
            'voucherHolder' => 'HuTao',
            'entryDate' => '2021-03-02',
            'nameOne' => 'Zhong Li',
            'nameTwo' => null,
            'address' => '124 Conch St., Bikini Bottom',
            'email' => '0001@gmail.com',
            'referenceNo' => 'WSFP0001',
            'receipt' => '',
            'contactNo' => '0123456789',
            'selectedOffer' => 'Trip to Liyue',
            'status' => 'NEW BOOKING',
            'statusNote' => '',
            'paymentType' => 'OFFLINE',
            'bookingDate' => '2021-04-16',
            'dateOne' => '2021-04-16',
            'dateTwo' => '',
            'dateThree' => '',
            'chosenOutlet' => 'Liyue',
            'confirmedOutlet' => 'Wangshu Inn',
            'dueDate' => '2021-04-16',
            'confirmedDate' => '2021-06-24',
            'confirmedLocation' => 'Liyue',
            'paymentAmount' => '150.00',
            'hotelUpgrade' => 'false',
            'roomUpgrade' => 'false',
            'specialRequest' => '',
            'emailSentDate' => '04-03-2021',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'last_edited_by' => 'admin77@gmail.com'
        ]);

        DB::table('client')->insert([
            'clientName' => 'Wangsheng',
            'clientType' => 'CORPORATE',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_by' => 'admin77@gmail.com',
            'last_edited_by' => 'admin77@gmail.com'
        ]);

        DB::table('group')->insert([
            'groupName' => 'HOLIDAY',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_by' => 'admin77@gmail.com',
            'last_edited_by' => 'admin77@gmail.com'
        ]);

        DB::table('hotel')->insert([
            'hotelName' => 'Wangshu Inn',
            'hotelBank' => 'Northland Bank',
            'hotelBankAcc' => '155288322',
            'picName' => 'Verr Goldet',
            'picEmail' => 'vr@wangshu.com',
            'hotelContact' => '0123456789',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_by' => 'admin77@gmail.com',
            'last_edited_by' => 'admin77@gmail.com'
        ]);

        DB::table('invoice')->insert([
            'bookingRefNo' => 'WSFP0001',
            'voucherNo' => 'TTL 0001',
            'hotelName' => 'Wangshu Inn',
            'customerName' => 'HuTao',
            'checkInDate' => '2021-06-24',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_by' => 'admin77@gmail.com',
            'last_edited_by' => 'admin77@gmail.com'
        ]);

        
        DB::table('offer')->insert([
            'referenceName' => '2 Days 1 Night Trip to Liyue',
            'offerName' => 'Trip to Liyue',
            'status' => 'ACTIVE',
            'description' => 'Overseas trip to Liyue',
            'termsConditions' => 'Valid Passport',
            'thumbnail' => '',
            'banner' => '',
            'bookingForm' => '',
            'payment' => 'false',
            'bookingRate' => '150',
            'locations' => '["Liyue"]',
            'accommodations' => '',
            'groupType' => 'HOLDIAY',
            'expiryDate' => '2021-02-13',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_by' => 'admin77@gmail.com',
            'last_edited_by' => 'admin77@gmail.com'
        ]);

        DB::table('project')->insert([
            'clientName' => 'Wangsheng',
            'projectName' => 'Trip to Liyue',
            'projectPrefix' => 'TTL',
            'title' => 'Liyue Trip',
            'greetingNote' => 'Welcome to Liyue',
            'greetingBanner' => '',
            'clientLogo' => '',
            'expiryDate' => '2021-05-07',
            'offers' => '["2 Days 1 Night Trip to Liyue"]',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_by' => 'admin77@gmail.com',
            'last_edited_by' => 'admin77@gmail.com'
        ]);

        DB::table('voucher')->insert([
            'clientName' => 'Wangsheng',
            //'projectName' => 'Trip to Liyue',
            'voucherNo' => 'TTL 0001',
            'activationCode' => '2CSOQ',
            'expiryDate' => '2021-05-06',
            'dateLimiter' => '-3 week',
            'status' => 'assigned',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_by' => 'admin77@gmail.com',
            'last_edited_by' => 'admin77@gmail.com'
        ]);
    }
}
