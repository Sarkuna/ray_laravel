@extends('layouts.dashboard')  
@section('content')


<div class="content-wrapper">
@include('inc.messages')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Offer Expired</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Project</a></li>
                        <li class="breadcrumb-item active">Offer</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <section class="content">
        <div class="container-fluid">
            <hr>
            <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <a href="/offer/create" class="btn btn-primary">Create Offer</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <table id="example1" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
                    <thead>
                        <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="No.: activate to sort column descending" style="width:5%">No.</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Reference Name: activate to sort column ascending">Reference Name</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Offer Name: activate to sort column ascending">Offer Name</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending">Status</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Action: activate to sort column ascending">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($offerData as $offer)
                            <tr role="row" class="odd">
                                <td class="dtr-control sorting_1" tabindex="0">{{$offer->id}}</td>
                                <td>{{$offer->referenceName}}</td>
                                <td>{{$offer->offerName}}</td>
                                <td>{{$offer->status}}</td>
                                <td>
                                    <div class="row">
                                        <a href="/offer/{{$offer->id}}/edit" class="btn btn-primary">Edit</a>&nbsp;
                                        {!!Form::open(['action' => ['OfferController@destroy', $offer->id], 'method' => 'POST', 'class' => 'item-btn'])!!}
                                            <input name="_method" type="hidden" value="DELETE">
                                            <input class="btn btn-danger" type="submit" value="Delete">
                                        {!!Form::close()!!}
                                    </div>
                                    
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
@endsection