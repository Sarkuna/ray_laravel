@extends('layouts.dashboard')  
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Project/Offer</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
                        <li class="breadcrumb-item"><a href="/project/offer">Project/Offer</a></li>
                        <li class="breadcrumb-item active">Edit Offer</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <section class="content">
        <div class="container-fluid">
            <!-- general form elements -->
            <div class="row">
                <div class="col-sm">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Edit Offer</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            {!! Form::open(['action' => ['OfferController@update', $offerData->id], 'method' => 'POST', 'files' => true]) !!}
                                <div class="form-group">
                                    <label>Reference Name:</label>
                                    <input type="text" name="referenceName" class="form-control form-control-border" value="{{$offerData->referenceName}}">
                                </div>
                                <div class="form-group">
                                    <label>Offer Name:</label>
                                    <input type="text" name="offerName" class="form-control form-control-border border-width-2" value="{{$offerData->offerName}}">
                                </div>
                                <div class="form-group">
                                    <label>Offer Status:</label>
                                    <select class="custom-select form-control-border" name="status">
                                        <option disabled value>-- Please set the Offer Status --</option>
                                        <option value="ACTIVE" <?php if($offerData->status == "ACTIVE") echo "selected"; ?>>Active</option>
                                        <option value="INACTIVE" <?php if($offerData->status == "INACTIVE") echo "selected"; ?>>Inactive</option>
                                        <option value="EXPIRED" <?php if($offerData->status == "EXPIRED") echo "selected"; ?>>Expired</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Description:</label>
                                    <textarea name="description" class="ckeditor form-control">{{$offerData->description}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label>Terms and Condition:</label>
                                    <textarea name="termsAndCondition" class="ckeditor form-control">{{$offerData->termsConditions}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label>Thumbnail(Optional):</label>
                                    @if ($offerData->thumbnail == null)
                                        <p>No thumbnail was uploaded</p>
                                    @else
                                        <p>Current thumbnail:</p><img src="/images/offerThumbnail/{{$offerData->thumbnail}}"><br><br>
                                    @endif
                                    <input type="file" name="thumbnail" class="form-control" value="{{$offerData->thumbnail}}">
                                </div>
                                <div class="form-group">
                                    <label>Banner(Optional):</label>
                                    @if ($offerData->banner == null)
                                        <p>No banner was uploaded</p>
                                    @else
                                        <p>Current banner:</p><img src="/images/offerBanner/{{$offerData->banner}}"><br><br>
                                    @endif
                                    <input type="file" name="banner" class="form-control" value="{{$offerData->banner}}">
                                </div>
                                <div class="form-group">
                                    <label>Booking Form:</label>
                                    <select class="custom-select form-control-border" name="bookingForm">
                                        <option disabled selected value>-- Please select a Booking Form --</option>
                                        @foreach ($bookingFormData as $form)
                                            <option value="{{$form->type}}" <?php if($offerData->bookingForm == "$form->type") echo "selected"; ?> >{{$form->type}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Group:</label>
                                    <select class="custom-select form-control-border" name="groupType">
                                        <option disabled selected value>-- Please select a Group --</option>
                                        @foreach ($groupData as $group)
                                            <option value="{{$group->groupName}}" <?php if($offerData->groupType == "$group->groupName") echo "selected"; ?> >{{$group->groupName}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Booking Requires Payment?:</label>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="bookingPayment" value="true">
                                        <label class="form-check-label">Yes</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="bookingPayment" value="false" checked="">
                                        <label class="form-check-label">No</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Offer Expiry Date:</label>
                                    <input type="date" name="expiryDate" class="form-control form-control-border border-width-2" value="{{$offerData->expiryDate}}">
                                </div>
                                <div class="form-group">
                                    <label>Booking Rate (RM):</label>
                                    <input class="form-control" type="text" name="bookingRate" value="{{$offerData->bookingRate}}">
                                </div>
                                <div class="form-group">
                                    <label>Location(s):</label>
                                    <div class="field_wrapper">
                                        @foreach ($offerData->locations as $locations)
                                            <div>
                                                <input type="text" name="locations[]" value={{$locations}} class="form-control">
                                            </div>
                                        @endforeach
                                    </div>
                                    <a href="javascript:void(0);" class="add_button" title="Add field"><i class="fas fa-plus-square"></i></a>
                                </div>
                                <!-- /.card-body -->
                                <input type="hidden" value="PUT" name="_method">
                                <input type="submit" value="Submit" class="btn btn-primary"> 
                            {!! Form::close() !!}
                        </div>
                    <!-- /.card -->
                    </div>
                </div>
                <div class="col">
                    @if($errors->any())
                        <div class="card card-danger sticky" style="width:40%">
                            <div class="card-header">
                                <h3 class="card-title">Errors</h3>
                            </div>
                            <div class="card-body">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
</div>
@endsection