@extends('layouts.dashboard')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Administrative Account</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
                        <li class="breadcrumb-item active">Adminstative Account</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <section class="content">
        <div class="container-fluid">
            <hr>
            <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <!--Modal-->
                        <button type="button" class="btn btn-primary item-btn" data-toggle="modal" data-target="#myModal">
                            Create Admin
                        </button>
                        <!--The Modal Container-->
                        <div class="modal fade" id="myModal">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <!--Modal Header-->
                                    <div class="modal-header">
                                    <h4 class="modal-title">Create Admin</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <!--Modal body-->
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <div class="section">
                                                {!! Form::open(['action' => 'AdminController@store', 'method' => 'POST']) !!}
                                                    <div class="form-group">
                                                        <label>Username:</label>
                                                        <input type="text" name="username" class="form-control form-control-border border-width-2">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Password:</label>
                                                        <input type="password" name="password" class="form-control form-control-border border-width-2">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Email:</label>
                                                        <input type="email" name="email" class="form-control form-control-border border-width-2">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Account Level:</label>
                                                        <select name="accType" class="custom-select form-control-border">
                                                            <option>Please Select a Level</option>
                                                            <option value="admin">Admin</option>
                                                            <option value="cs">Customer Support</option>
                                                            <option value="intern">Intern</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Permissions:</label>
                                                        <div class="field_wrapper">
                                                            <div>
                                                                <input type="text" name="permissions[]" value="" class="form-control">
                                                            </div>
                                                        </div>
                                                        <a href="javascript:void(0);" class="add_button" title="Add field"><i class="fas fa-plus-square"></i></a>
                                                    </div>
                                                    <input type="submit" value="Submit" class="btn btn-primary"> 
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                    <!--Modal footer-->
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--End of Modal-->
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <table id="example1" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
                        <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="No.: activate to sort column descending">No.</th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Email: activate to sort column ascending">Email</th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Account Level: activate to sort column ascending">Account Level</th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Account Status: activate to sort column ascending">Account Status</th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Created at: activate to sort column ascending">Created at</th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Action: activate to sort column ascending">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($userData as $user)
                                <tr role="row" class="odd">
                                    <td class="dtr-control sorting_1" tabindex="0">{{$user->id}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->acctype}}</td>
                                    <td>{{$user->status}}</td>
                                    <td>{{$user->created_at}}</td>
                                    <td>
                                        <!--Modal-->
                                        <button type="button" class="btn btn-primary item-btn" data-toggle="modal" data-target="#myModal2">
                                            Edit Admin
                                        </button>
                                        <!--The Modal Container-->
                                        <div class="modal fade" id="myModal2">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <!--Modal Header-->
                                                    <div class="modal-header">
                                                    <h4 class="modal-title">Edit Admin</h4>
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    </div>
                                                    <!--Modal body-->
                                                    <div class="modal-body">
                                                        <div class="form-group">
                                                            <div class="section">
                                                            {!! Form::open(['action' => ['AdminController@update', $user->id], 'method' => 'POST']) !!}
                                                                <div class="form-group">
                                                                    <label>Username:</label>
                                                                    <input type="text" name="username" class="form-control form-control-border border-width-2" value="{{$user->name}}">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Password:</label>
                                                                    <input type="password" name="password" class="form-control form-control-border border-width-2">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Email:</label>
                                                                    <p>{{$user->email}}</p>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Account Level:</label>
                                                                    <select name="accType" class="custom-select form-control-border">
                                                                        <option>Please Select a Level</option>
                                                                        <option value="Admin" <?php if($user->acctype == "admin") echo "selected"; ?>>Admin</option>
                                                                        <option value="Customer Support" <?php if($user->acctype == "Customer Support") echo "selected"; ?>>Customer Support</option>
                                                                        <option value="Intern" <?php if($user->acctype == "Intern") echo "selected"; ?>>Intern</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Account Status:</label>
                                                                    <select name="status" class="custom-select form-control-border">
                                                                        <option>Please choose a status</option>
                                                                        <option value="active" <?php if($user->status == "active") echo "selected"; ?>>Active</option>
                                                                        <option value="inactive" <?php if($user->status == "inactive") echo "selected"; ?>>Inactive</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Permissions:</label>
                                                                    <div class="field_wrapper">
                                                                        <div>
                                                                            <input type="text" name="permissions[]" value="" class="form-control">
                                                                        </div>
                                                                    </div>
                                                                    <a href="javascript:void(0);" class="add_button" title="Add field"><i class="fas fa-plus-square"></i></a>
                                                                </div>
                                                                <input type="hidden" value="PUT" name="_method">
                                                                <input type="submit" value="Submit" class="btn btn-primary"> 
                                                            {!! Form::close() !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--Modal footer-->
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--End of Modal-->
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>    
                    </table>
                </div>
            </div>
            <!-- /.card-body --> 
        </div>
    </section>
</div>
@endsection