@extends('layouts.dashboard')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header no-printMe">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Payment Receipt</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
                        <li class="breadcrumb-item active">Payment Receipt</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <section class="content"><!-- Body -->
    <div class="container-fluid">
        <hr>
        @if($errors->any())
            <div class="alert alert-danger">
                Failed to send/store voucher because:
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <!-- /.card-header -->
        <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                </div>
            </div>
        </div>
    </div>
        <div class="row">
            <div class="col-sm-12">
                <table id="example1" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
                    <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Voucher No.: activate to sort column descending">Voucher No.</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Voucher Holder: activate to sort column ascending">Voucher Holder</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Bookign Date: activate to sort column ascending">Booking Date</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Payment Type: activate to sort column ascending">Payment Type</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Receipt: activate to sort column ascending">Booking Receipt</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Receipt: activate to sort column ascending">Payment Receipt</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending">Status</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Amount: activate to sort column ascending">Amount</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Due Date: activate to sort column ascending">Due Date</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Action: activate to sort column ascending">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($jointData as $joint)
                        <tr role="row" class="odd">
                            <td class="dtr-control sorting_1" tabindex="0">
                                <!--Modal-->
                                 <a href="#" data-toggle="modal" data-target="#myModal">
                                    {{$joint->voucherNo}}
                                </a>
                                <!--The Modal Container-->
                                <div class="modal fade" id="myModal">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <!--Modal Header-->
                                            <div class="modal-header">
                                            <h4 class="modal-title">joint Details for {{$joint->voucherNo}}</h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <!--Modal body-->
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <div class="section">
                                                    <b>Name 1: </b>{{$joint->nameOne}}<br>
                                                    <b>Name 2: </b>{{$joint->nameTwo}}<br>
                                                    <b>Address: </b>{{$joint->address}}<br>
                                                    <b>Email: </b>{{$joint->email}}<br>
                                                    <b>Contact: </b>{{$joint->contactNo}}<br>
                                                    <b>Date 1: </b>{{$joint->dateOne}}<br>
                                                    <b>Date 2: </b>{{$joint->dateTwo}}<br>
                                                    <b>Date 3: </b>{{$joint->dateThree}}<br>
                                                    <b>Selected Offer: </b>{{$joint->selectedOffer}}<br>
                                                    <b>Choosen Outlet/Destination: </b>{{$joint->chosenOutlet}}
                                                    <hr>
                                                    <b>Entry Date: </b>{{$booking->entryDate}}
                                                    <b>Email Sent Date: </b>{{$booking->emailSentDate}}
                                                    </div>
                                                </div>
                                            </div>
                                            <!--Modal footer-->
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--End of Modal-->
                            </td>
                            <td>{{$joint->voucherHolder}}</td>
                            <td>{{$joint->bookingDate}}</td>
                            <td>{{$joint->paymentType}}</td>
                            <td>
                                @if($joint->paymentType = "offline")
                                    <!--Receipt Modal 1-->
                                    @if($joint->receipt != null)
                                        <!--Receipt Modal-->
                                        <a href="#" data-toggle="modal" data-target="#receiptModal">
                                            {{$joint->receipt}}
                                        </a>
                                    @else
                                        <a href="#" data-toggle="modal" data-target="#receiptModal">
                                            Upload Receipt
                                        </a>
                                    @endif
                                    <!--The Modal Container-->
                                    <div class="modal fade" id="receiptModal">
                                        <div class="modal-dialog modal-dialog-centered">
                                            {!!Form::open(['action' => ['DatabaseController@uploadReceipt', $joint->id], 'method' => 'POST', 'files' => true, 'class' => 'item-btn'])!!}
                                                <div class="modal-content">
                                                    <!--Modal Header-->
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Receipt for {{$joint->voucherNo}}</h4>
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    </div>
                                                    <!--Modal body-->
                                                    <div class="modal-body">
                                                        <div class="form-group">
                                                            <div class="section">
                                                                    <label>Receipt:</label><br>
                                                                    @if($joint->receipt != null)
                                                                        <img src="/images/receipt/{{$joint->receipt}}"><br><br>
                                                                    @else
                                                                        <p>No receipt available</p>
                                                                    @endif
                                                                    <input type="file" name="receiptImage" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--Modal footer-->
                                                    <div class="modal-footer">
                                                        <input type="hidden" name="bookingId" value="{{$joint->id}}">
                                                        <button type="submit" class="btn btn-primary">Upload</button>
                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            {!!Form::close()!!}
                                        </div>
                                    </div>
                                    <!--End of Modal-->
                                @else
                                    <p>Online Payment</p>
                                @endif
                            </td>
                            <td>
                                <!--Receipt Modal 2-->
                                @if($joint->paymentReceipt != null)
                                    <!--Receipt Modal-->
                                    <a href="#" data-toggle="modal" data-target="#paymentModal">
                                        {{$joint->paymentReceipt}}
                                    </a>
                                @else
                                    <a href="#" data-toggle="modal" data-target="#paymentModal">
                                        Upload Receipt
                                    </a>
                                @endif
                                <!--The Modal Container-->
                                <div class="modal fade" id="paymentModal">
                                    <div class="modal-dialog modal-dialog-centered">
                                        {!!Form::open(['action' => ['DatabaseController@uploadPaymentReceipt', $joint->id], 'method' => 'POST', 'files' => true, 'class' => 'item-btn'])!!}
                                            <div class="modal-content">
                                                <!--Modal Header-->
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Payment Receipt for {{$joint->voucherNo}}</h4>
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                </div>
                                                <!--Modal body-->
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <div class="section">
                                                                <label>Receipt:</label><br>
                                                                @if($joint->paymentReceipt != null)
                                                                    <img src="/images/receipt/{{$joint->paymentReceipt}}"><br><br>
                                                                @else
                                                                    <p>No receipt available</p>
                                                                @endif
                                                                    <input type="file" name="paymentReceiptImage" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Modal footer-->
                                                <div class="modal-footer">
                                                    <input type="hidden" name="bookingId" value="{{$joint->booking_id}}">
                                                    <input type="hidden" name="voucherNo" value="{{$joint->voucherNo}}">
                                                    <button type="submit" class="btn btn-primary">Upload</button>
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        {!!Form::close()!!}
                                    </div>
                                </div>
                                <!--End of Modal-->
                            </td>
                            <td>{{$joint->status}}</td>
                            <td>RM {{$joint->paymentAmount}}</td>
                            <td>{{$joint->dueDate}}</td>
                            <td>
                                <div class="row">
                                    <a href="/joint/{{$joint->id}}/edit" class="btn btn-primary">Edit</a>&nbsp;
                                    <button class="btn btn-danger" data-toggle="modal" data-target="#deleteConfirmModal">Cancel</button>&nbsp;
                                    {!!Form::open(['action' => ['BookingController@destroy', $booking->id], 'method' => 'POST', 'class' => 'item-btn'])!!}
                                        <div class="modal fade" id="deleteConfirmModal">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Confirm Deletion of Voucher {{$booking->voucherNo}}?</h4>
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <input name="_method" type="hidden" value="DELETE">
                                                        <input class="btn btn-danger" type="submit" value="Yes, Cancel">
                                                        <button type="button" class="btn btn-primary" data-dismiss="modal">No, Close Window</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    {!!Form::close()!!}
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.card-body --> 
    </div>
    </section>
</div>
@endsection