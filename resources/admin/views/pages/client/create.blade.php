@extends('layouts.dashboard')  
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Project/Client</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
                        <li class="breadcrumb-item"><a href="/project/main">Project</a></li>
                        <li class="breadcrumb-item active">Create Client</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <section class="content">
        <div class="container-fluid">
            <!-- general form elements -->
            <div class="row">
                <div class="col-sm">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Create Client</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            {!! Form::open(['action' => 'ClientController@store', 'method' => 'POST', 'files' => true]) !!}
                                <div class="form-group">
                                    <label>Client Name:</label>
                                    <input type="text" name="clientName" class="form-control form-control-border border-width-2" placeholder="Client Name">
                                </div>
                                <div class="form-group">
                                    <label>Client Type:</label>
                                    <select class="custom-select form-control-border" name="clientType">
                                        <option disabled selected value>-- Please select a Client Type--</option>
                                        <option value="INDIVIDUAL">Individual</option>
                                        <option value="CORPORATE">Corporate</option>
                                    </select>
                                </div>
                                <!-- /.card-body -->
                                <input type="submit" value="Submit" class="btn btn-primary"> 
                            {!! Form::close() !!}
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <div class="col">
                    @if($errors->any())
                        <div class="card card-danger sticky" style="width:40%">
                            <div class="card-header">
                                <h3 class="card-title">Errors</h3>
                            </div>
                            <div class="card-body">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
</div>
@endsection