@extends('layouts.dashboard')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Voucher/Generate</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
                        <li class="breadcrumb-item"><a href="/voucher/main">voucher</a></li>
                        <li class="breadcrumb-item active">Generate Voucher</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <section class="content"><!-- Body -->
    <div class="container-fluid">
        <hr>
        @if($errors->any())
            <div class="alert alert-danger">
                Failed to generate vouchers please click on "Generate Vouchers" to see why.
            </div>
        @endif
        <!-- /.card-header -->
        <div class="card-body">
            <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <div class="dt-buttons btn-group flex-wrap">               
                            <button type="button" class="btn btn-primary item-btn" data-toggle="modal" data-target="#myModal">
                                Generate Vouchers
                            </button>
                            <!--The Modal Container-->
                            <div class="modal fade" id="myModal">
                                <!-- <div class="row">
                                    <div class="col"> -->
                                        <div class="modal-dialog modal-dialog-centered modal-xl">
                                            <div class="modal-content">
                                                <!--Modal Header-->
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Generate Vouchers</h4>
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                </div>
                                                <div class="row">
                                                    <div class="col">
                                                        <!--Modal body-->
                                                        <div class="modal-body">
                                                        {!! Form::open(['action' => 'VoucherController@store', 'method' => 'POST']) !!}
                                                            <div class="form-group">
                                                                <div class="section">
                                                                    <div class="form-group">
                                                                        <label>Project:</label>
                                                                        <select name="projectPrefix" class="custom-select form-control-border" onchange="addText(this.value)">
                                                                            <option disabled selected value>-- Please select a project --</option>
                                                                            @foreach($projectData as $project)
                                                                                <option value="{{$project->projectPrefix}}">{{$project->projectName}} ({{$project->projectPrefix}})</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Client:</label>
                                                                        <select name="clientName" class="custom-select form-control-border">
                                                                            <option disabled selected value>-- Please select a client --</option>
                                                                            @foreach($clientData as $client)
                                                                                <option value="{{$client->clientName}}">{{$client->clientName}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>No of Vouchers:</label>
                                                                        <input type="number" name="totalVouchers" class="form-control form-control-border border-width-2">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Starting Number:</label>
                                                                        <input type="text" name="startingNumber" class="form-control form-control-border border-width-2" id="startingPrefix">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Expiry Date:</label>
                                                                        <input type="date" name="expiryDate" class="form-control form-control-border border-width-2" min={{date('Y-m-d',strtotime('now'))}}>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Date Limiter:</label>
                                                                        <div class="row">
                                                                            <select name="number" class="custom-select form-control-border" style="width:50%">
                                                                                <option disabled selected value>-- Please select the duration --</option>
                                                                                <option value="1">1</option>
                                                                                <option value="2">2</option>
                                                                                <option value="3">3</option>
                                                                                <option value="4">4</option>
                                                                                <option value="5">5</option>
                                                                                <option value="6">6</option>
                                                                                <option value="7">7</option>
                                                                                <option value="8">8</option>
                                                                                <option value="9">9</option>
                                                                                <option value="10">10</option>
                                                                                <option value="11">11</option>
                                                                                <option value="12">12</option>
                                                                            </select>

                                                                            <select name="timeUnit" class="custom-select form-control-border" style="width:50%">
                                                                                <option disabled selected value>-- Please select a time unit--</option>
                                                                                <option value="week">week</option>
                                                                                <option value="month">month</option>
                                                                            </select>
                                                                        </div>
                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--Modal footer-->
                                                        <div class="modal-footer">
                                                            <input type="submit" value="Submit" class="btn btn-primary"> 
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                        </div>
                                                        {!! Form::close() !!}
                                                    </div>
                                                    @if($errors->any())
                                                        <div class="col">
                                                            <div class="modal-header">
                                                                <h3 class="modal-title">Errors</h3>
                                                            </div>
                                                            <ul>
                                                                @foreach($errors->all() as $error)
                                                                    <li>{{$error}}</li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                   
                                <!-- </div>
                            </div> -->
                            <!--End of Modal-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <table id="example1" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
                        <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="No.: activate to sort column descending">No.</th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Client: activate to sort column ascending">Client</th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Project: activate to sort column ascending">Project</th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Creation Date/Time: activate to sort column ascending">Creation Date/Time</th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Expiry Date: activate to sort column ascending">Expiry Date</th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="No of Vouchers: activate to sort column ascending">No of Vouchers</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($voucherData as $voucher)
                            <tr role="row" class="odd">
                                <td class="dtr-control sorting_1" tabindex="0">{{$voucher->id}}</td>
                                <td>{{$voucher->clientName}}</td>
                                <td>{{$voucher->projectName}}</td>
                                <td>
                                    <b>Date & Time: </b>{{$voucher->created_at}}
                                </td>
                                <td>
                                    <p>{{$voucher->expiryDate}}</p>
                                </td>
                                <td>
                                    <b>Total: </b>0<br>
                                    <b>Assigned: </b>0<br>
                                    <b>Unassigned: </b>0<br>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>    
                    </table>
                </div>
            </div>
        </div>
        <!-- /.card-body --> 
    </div>
    </section>
</div>

<script>
    // function addText(val){
    //         $('#startingPrefix').val(val + " ");
    //     }
</script>
@endsection
