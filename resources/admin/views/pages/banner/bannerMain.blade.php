@extends('layouts.dashboard')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Banner</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
                        <li class="breadcrumb-item active">Banner</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <section class="content">
    <div class="container-fluid">
    <hr>
        <!-- /.card-header -->
        <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                <a href="/banner/create" class="btn btn-primary">Add Banner</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <table id="example1" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
                    <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="No.: activate to sort column descending">No.</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Banner Image: activate to sort column ascending">Banner Image</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Show in: activate to sort column ascending">Show in</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Alignment activate to sort column ascending">Alignment</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Banner Link: activate to sort column ascending">Banner Link</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Action: activate to sort column ascending">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($bannerData as $banner)
                            <tr role="row" class="odd">
                                <td class="dtr-control sorting_1" tabindex="0">{{$banner->id}}</td>
                                <td>
                                    <!--Modal-->
                                    <button type="button" class="btn btn-primary item-btn" data-toggle="modal" data-target="#myModal" data-p_name="{{$banner->id}}">
                                        View banner image
                                    </button>
                                    <!--The Modal Container-->
                                    <div class="modal fade" id="myModal">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <!--Modal Header-->
                                                <div class="modal-header">
                                                <h4 class="modal-title">Banner Image</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                </div>
                                                <!--Modal body-->
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <div class="section">
                                                            <img src="/images/banners/{{$banner->alignment}}/{{$banner->bannerImage}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Modal footer-->
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--End of Modal-->
                                </td>
                                <td>{{$banner->showIn}}</td>
                                <td>{{$banner->alignment}}</td>
                                <td>{{$banner->bannerLink}}</td>
                                <td>
                                    <div class="row">
                                        <a href="/banner/{{$banner->id}}/edit" class="btn btn-primary">Edit</a>&nbsp;
                                        {!!Form::open(['action' => ['BannerController@destroy', $banner->id], 'method' => 'POST', 'class' => 'item-btn'])!!}
                                            <input name="_method" type="hidden" value="DELETE">
                                            <input class="btn btn-danger" type="submit" value="Delete">
                                        {!!Form::close()!!}
                                    </div>
                                    
                                </td>
                            </tr>
                        @endforeach
                    </tbody>    
                </table>
            </div>
        </div>
        <!-- /.card-body --> 
    </div>
    </section>
</div>


@endsection