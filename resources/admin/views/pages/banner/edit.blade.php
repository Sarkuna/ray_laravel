@extends('layouts.dashboard')  

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Banner</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
                        <li class="breadcrumb-item"><a href="/banner/main">Banner</a></li>
                        <li class="breadcrumb-item active">Add Banner</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <section class="content">
        <div class="container-fluid">
            <!-- general form elements -->
            <div class="card card-primary" style="width:50%">
                <div class="card-header">
                    <h3 class="card-title">Edit Banner</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    {!! Form::open(['action' => ['BannerController@update', $bannerData->id], 'method' => 'POST', 'files' => true]) !!}
                        <div class="form-group">
                            <label>Show in:</label>
                            <select class="custom-select form-control-border" name="showIn">
                                <option value="allPages" <?php if($bannerData->showIn == "allPages") echo "selected"; ?> >All Pages</option>
                                <option value="homepage" <?php if($bannerData->showIn == "homepage") echo "selected"; ?> >Homepage</option>
                                <option value="subPages" <?php if($bannerData->showIn == "subPages") echo "selected"; ?> >Sub Page</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Banner Link (Optional):</label>
                            <input type="text" name="bannerLink" class="form-control form-control-border border-width-2" value="{{$bannerData->bannerLink}}">
                        </div>
                        <div class="form-group">
                            <label>Banner:</label><br>
                            <img src="/images/banners/{{$bannerData->alignment}}/{{$bannerData->bannerImage}}"><br><br>
                            <input type="file" name="bannerImage" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Alignment:</label>
                            <select name="alignment" class="custom-select form-control-border">
                                <option value="topBanner" <?php if($bannerData->alignment == "topBanner") echo "selected"; ?> >Top Banner</option>
                                <option value="homepageBanner" <?php if($bannerData->alignment == "homepageBanner") echo "selected"; ?> >Homepage Banner</option>
                                <option value="leftBanner" <?php if($bannerData->alignment == "leftBanner") echo "selected"; ?> >Left Banner</option>
                                <option value="rightBanner" <?php if($bannerData->alignment == "rightBanner") echo "selected"; ?> >Right Banner</option>
                                <option value="bottomBanner" <?php if($bannerData->alignment == "bottomBanner") echo "selected"; ?> >Bottom Banner</option>
                            </select>
                            <p>
                            Dimensions pixel(px) by pixel(px):<br>
                            Top: 728x90<br>
                            Homepage: 693x399<br>
                            Left: 160x600<br>
                            Right: 200x200<br>
                            Bottom: 728x90
                            </p>
                       </div>
                       <div class="form-group">
                            <label>Banner Note:</label>
                            <textarea class="ckeditor form-control" name="bannerNote">{{$bannerData->bannerNote}}</textarea>
                       </div>
                        <!-- /.card-body -->
                        <input type="hidden" value="PUT" name="_method">
                        <input type="submit" value="Submit" class="btn btn-primary"> 
                    {!! Form::close() !!}
                </div>
                <!-- /.card -->
            </div>
        </div>
    </section>
</div>
@endsection