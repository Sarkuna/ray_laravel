@extends('layouts.dashboard')  
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Hotel Template</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
                        <li class="breadcrumb-item"><a href="/template/hotel">Hotel Template</a></li>
                        <li class="breadcrumb-item active">Create Template</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <section class="content">
        <div class="container-fluid">
            <!-- general form elements -->
            <div class="card card-primary" style="width:50%">
                <div class="card-header">
                    <h3 class="card-title">Create Hotel Template</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    {!! Form::open(['action' => ['HotelController@store', $hotelData->id], 'method' => 'POST']) !!}
                        <div class="form-group">
                            <label>Hotel Name:</label>
                            <input type="text" name="hotelName" class="form-control form-control-border border-width-2" value="{{$hotelData->hotelName}}">
                        </div>
                        <div class="form-group">
                            <label>Hotel Bank Name:</label>
                            <input type="text" name="hotelBank" class="form-control form-control-border border-width-2" value="{{$hotelData->hotelBank}}">
                        </div>
                        <div class="form-group">
                            <label>Hotel Bank Account No.:</label>
                            <input type="text" name="hotelBankAcc" class="form-control form-control-border border-width-2" value="{{$hotelData->hotelBankAcc}}">
                        </div>
                        <div class="form-group">
                            <label>PIC Name:</label>
                            <input type="text" name="picName" class="form-control form-control-border border-width-2" value="{{$hotelData->picName}}">
                        </div>
                        <div class="form-group">
                            <label>PIC Email:</label>
                            <input type="text" name="picEmail" class="form-control form-control-border border-width-2" value="{{$hotelData->picEmail}}">
                        </div>
                        <div class="form-group">
                            <label>Hotel Contact No.:</label>
                            <input type="text" name="hotelContact" class="form-control form-control-border border-width-2" value="{{$hotelData->hotelContact}}">
                        </div>
                        <!-- /.card-body -->
                        <input type="hidden" value="PUT" name="_method">
                        <input type="submit" value="Submit" class="btn btn-primary"> 
                    {!! Form::close() !!}
                </div>
                <!-- /.card -->
            </div>
        </div>
    </section>
</div>
@endsection