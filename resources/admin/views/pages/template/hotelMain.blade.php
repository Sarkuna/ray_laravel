@extends('layouts.dashboard')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Hotel Template</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
                        <li class="breadcrumb-item active">Hotel Template</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <section class="content"><!-- Body -->
    <div class="container-fluid">
        <hr>
        <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                <a href="/hotelTemplate/create" class="btn btn-primary">Create Template</a>
                </div>
            </div>
        </div>
        <!-- /.card-header -->
        <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                </div>
            </div>
        </div>
    </div>
        <div class="row">
            <div class="col-sm-12">
                <table id="example1" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
                    <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Hotel Name: activate to sort column descending">Hotel Name</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Hotel Bank Name: activate to sort column ascending">Hotel Bank Name</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Hotel Bank Account No: activate to sort column ascending">Hotel Bank Account No</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="PIC Name: activate to sort column ascending">PIC Name</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="PIC Email: activate to sort column ascending">PIC Email</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Hotel Contact No: activate to sort column ascending">Hotel Contact No</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Action: activate to sort column ascending">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($hotelData as $hotel)
                        <tr role="row" class="odd">
                            <td class="dtr-control sorting_1" tabindex="0">
                                {{$hotel->hotelName}}
                            </td>
                            <td>{{$hotel->hotelBank}}</td>
                            <td>{{$hotel->hotelBankAcc}}</td>
                            <td>{{$hotel->picName}}</td>
                            <td>{{$hotel->picEmail}}</td>
                            <td>{{$hotel->hotelContact}}</td>
                            <td>
                                <div class="row">
                                    <a href="/hotelTemplate/{{$hotel->id}}/edit" class="btn btn-primary">Edit</a>&nbsp;
                                    {!!Form::open(['action' => ['HotelController@destroy', $hotel->id], 'method' => 'POST', 'class' => 'item-btn'])!!}
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input class="btn btn-danger" type="submit" value="Delete">&nbsp;
                                    {!!Form::close()!!}
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.card-body --> 
    </div>
    </section>
</div>
@endsection