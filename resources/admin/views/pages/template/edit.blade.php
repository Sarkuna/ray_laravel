@extends('layouts.dashboard')  
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Email Template</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
                        <li class="breadcrumb-item"><a href="/template/email">Email Template</a></li>
                        <li class="breadcrumb-item active">Edit Template</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-8">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Edit Email Template</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            {!! Form::open(['action' => ['TemplateController@update', $templateData->id], 'method' => 'POST']) !!}
                                <div class="form-group">
                                    <label>Template Name:</label>
                                    <input type="text" name="templateName" class="form-control form-control-border border-width-2" value="{{$templateData->templateName}}">
                                </div>
                                <div class="form-group">
                                    <label>Template Description(optional):</label>
                                    <input type="text" name="templateDescription" class="form-control form-control-border border-width-2" placeholder="Template Description" value="{{$templateData->templateDescription}}">
                                </div>
                                <div class="form-group">
                                    <label>Hotel Email(optional):</label>
                                    <input type="text" name="hotelEmail" class="form-control form-control-border border-width-2" placeholder="Template Description" value="{{$templateData->hotelEmail}}">
                                </div>
                                <div class="form-group">
                                    <label>Template Content:</label>
                                    <textarea class="ckeditor form-control" name="templateContent">{{$templateData->templateContent}}</textarea>
                                </div>
                                <input type="hidden" value="PUT" name="_method">
                                <input type="submit" value="Submit" class="btn btn-primary"> 
                            {!! Form::close() !!}
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <div class="col">
                    <div class="card card-primary sticky">
                        <div class="card-header">
                            <h3 class="card-title">Available Tokens</h3>
                        </div>
                        <div class="card-body">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Tokens</th>
                                        <th>Description</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{cName}</td>
                                        <td>customer's name</td>
                                    </tr>
                                    <tr>
                                        <td>{cEmail}</td>
                                        <td>customer's email</td>
                                    </tr>
                                    <tr>
                                        <td>{cAddress}</td>
                                        <td>customer's address</td>
                                    </tr>
                                    <tr>
                                        <td>{cReferenceNo}</td>
                                        <td>customer's reference number</td>
                                    </tr>
                                    <tr>
                                        <td>{cContact}</td>
                                        <td>customer's contact number</td>
                                    </tr>
                                    <tr>
                                        <td>{cPaymentAmount}</td>
                                        <td>customer's payment Amount</td>
                                    </tr>
                                    <tr>
                                        <td>{hotelName}</td>
                                        <td>hotel's name</td>
                                    </tr>
                                    <tr>
                                        <td>{hotelBank}</td>
                                        <td>hotel's bank</td>
                                    </tr>
                                    <tr>
                                        <td>{hotelContact}</td>
                                        <td>hotel's contact number</td>
                                    </tr>
                                    <tr>
                                        <td>{picName}</td>
                                        <td>person in charge of hotel</td>
                                    </tr>
                                    <tr>
                                        <td>{picEmail}</td>
                                        <td>person in charge's email</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </section>
</div>
@endsection