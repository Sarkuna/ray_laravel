@extends('layouts.dashboard')  
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Hotel Template</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
                        <li class="breadcrumb-item"><a href="/template/hotel">Hotel Template</a></li>
                        <li class="breadcrumb-item active">Create Template</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <section class="content">
        <div class="container-fluid">
            <!-- general form elements -->
            <div class="row">
                <div class="col-sm">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Create Hotel Template</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            {!! Form::open(['action' => 'HotelController@store', 'method' => 'POST']) !!}
                                <div class="form-group">
                                    <label>Hotel Name:</label>
                                    <input type="text" name="hotelName" class="form-control form-control-border border-width-2" placeholder="Hotel Name">
                                </div>
                                <div class="form-group">
                                    <label>Hotel Bank Name:</label>
                                    <input type="text" name="hotelBank" class="form-control form-control-border border-width-2" placeholder="Hotel Bank Name">
                                </div>
                                <div class="form-group">
                                    <label>Hotel Bank Account No.:</label>
                                    <input type="text" name="hotelBankAcc" class="form-control form-control-border border-width-2" placeholder="Hotel Bank Account No.">
                                </div>
                                <div class="form-group">
                                    <label>PIC Name:</label>
                                    <input type="text" name="picName" class="form-control form-control-border border-width-2" placeholder="PIC Name">
                                </div>
                                <div class="form-group">
                                    <label>PIC Email:</label>
                                    <input type="text" name="picEmail" class="form-control form-control-border border-width-2" placeholder="PIC Email">
                                </div>
                                <div class="form-group">
                                    <label>Hotel Contact No.:</label>
                                    <input type="text" name="hotelContact" class="form-control form-control-border border-width-2" placeholder="Hotel Contact No.">
                                </div>
                                <!-- /.card-body -->
                                <input type="submit" value="Submit" class="btn btn-primary"> 
                            {!! Form::close() !!}
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <div class="col">
                    @if($errors->any())
                        <div class="card card-danger sticky" style="width:40%">
                            <div class="card-header">
                                <h3 class="card-title">Errors</h3>
                            </div>
                            <div class="card-body">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            
        </div>
    </section>
</div>
@endsection