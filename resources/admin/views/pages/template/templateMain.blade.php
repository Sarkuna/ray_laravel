@extends('layouts.dashboard')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Email Templates</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
                        <li class="breadcrumb-item active">Email Template</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <section class="content"><!-- Body -->
    <div class="container-fluid">
        <hr>
        <!-- /.card-header -->
        <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                <a href="/template/create" class="btn btn-primary">Create Template</a>
                </div>
            </div>
        </div>
    </div>
        <div class="row">
            <div class="col-sm-12">
                <table id="example1" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
                    <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="No.: activate to sort column descending">No.</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Template Name: activate to sort column ascending">Template Name</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Template Content: activate to sort column ascending">Template Description</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Action: activate to sort column ascending">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($templateData as $template)
                        <tr role="row" class="odd">
                            <td class="dtr-control sorting_1" tabindex="0">{{$template->id}}</td>
                            <td>{{$template->templateName}}</td>
                            <td>{{$template->templateDescription}}</td>
                            <td>
                                <div class="row">
                                    <a href="/template/{{$template->id}}/edit" class="btn btn-primary">Edit</a>&nbsp;
                                    {!!Form::open(['action' => ['TemplateController@destroy', $template->id], 'method' => 'POST', 'class' => 'item-btn'])!!}
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input class="btn btn-danger" type="submit" value="Delete">
                                    {!!Form::close()!!}
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.card-body --> 
    </div>
    </section>
</div>


@endsection