@extends('layouts.dashboard')  
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Booking</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
                        <li class="breadcrumb-item"><a href="/booking/main">Booking</a></li>
                        <li class="breadcrumb-item active">Edit Booking</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <section class="content">
        <div class="container-fluid">
            <!-- general form elements -->
            <div class="row">
                <div class="col-sm">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Edit Booking</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            {!! Form::open(['action' => ['BookingController@update', $bookingData->id], 'method' => 'POST']) !!}
                                <div class="form-group">
                                    <label>Name 1:</label>
                                    <input type="text" name="nameOne" class="form-control form-control-border border-width-2" value="{{$bookingData->nameOne}}">
                                </div>
                                <div class="form-group">
                                    <label>Name 2:</label>
                                    <input type="text" name="nameTwo" class="form-control form-control-border border-width-2" value="{{$bookingData->nameTwo}}">
                                </div>
                                <div class="form-group">
                                    <label>Address:</label>
                                    <textarea name="address" class="form-control">{{$bookingData->address}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label>Email:</label>
                                    <input type="text" name="email" class="form-control form-control-border border-width-2" value="{{$bookingData->email}}">
                                </div>
                                <div class="form-group">
                                    <label>Contact No:</label>
                                    <input type="text" name="contactNo" class="form-control form-control-border border-width-2" value="{{$bookingData->contactNo}}">
                                </div>
                                <!-- DateRangeLimiter Logic -->
                                @foreach($voucherData as $voucher)
                                    <div class="form-group">
                                        <label>Date 1:</label>
                                        <input type="date" name="dateOne" id="date1" class="form-control form-control-border border-width-2" value="{{$bookingData->dateOne}}" min={{date('Y-m-d',strtotime('now'))}} max={{$dateOneLimit}} onchange="dateOneSelected(this.value)">
                                    </div>
                                    <div class="form-group">
                                        <label>Date 2:</label>
                                        <input type="date" name="dateTwo" id="date2" class="form-control form-control-border border-width-2" value="{{$bookingData->dateTwo}}" min={{$dateTwoMinLimit}} max={{$dateTwoMaxLimit}}>
                                    </div>
                                    <div class="form-group">
                                        <label>Date 3:</label>
                                        <input type="date" name="dateThree" id="date3" class="form-control form-control-border border-width-2" value="{{$bookingData->dateThree}}" min={{$dateThreeMinLimit}} max={{$dateThreeMaxLimit}}>
                                    </div>
                                @endforeach
                                <!-- DateRangeLimiter ^^^ -->
                                <div class="form-group">
                                    <label>Confirmed Outlet/Destination:</label>
                                    <select class="custom-select form-control-border" name="confirmedOutlet">
                                        <option value="#">#</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Payment Status:</label>
                                    <select class="custom-select form-control-border" name="status">
                                        <option value="PENDING" <?php if($bookingData->status == "pending") echo "selected"; ?> >Pending</option>
                                        <option value="SUCCESSFUL" <?php if($bookingData->status == "successful") echo "selected"; ?> >Successful</option>
                                        <option value="ABORTED" <?php if($bookingData->status == "aborted") echo "selected"; ?> >Aborted</option>
                                        <option value="N/A" <?php if($bookingData->status == "n/a") echo "selected"; ?> >N/A</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Payment Status Note(optional):</label>
                                    <input type="text" name="statusNote" class="form-control form-control-border border-width-2" value="{{$bookingData->statusNote}}">
                                </div>
                                <div class="form-group">
                                    <label>Payment Due Date:</label>
                                    <input type="date" name="dueDate" class="form-control form-control-border border-width-2" value="{{$bookingData->dueDate}}">
                                </div>
                                <div class="form-group">
                                    <label>Payment Amount:</label>
                                    <input type="number" name="paymentAmount" class="form-control form-control-border border-width-2" value="{{$bookingData->paymentAmount}}">
                                </div>
                                <div class="form-group">
                                    <label>Confirmed Date:</label>
                                    <input type="date" name="confirmedDate" class="form-control form-control-border border-width-2" value="{{$bookingData->confirmedDate}}">
                                </div>
                                <div class="form-group">
                                    <label>Remarks:</label>
                                    <textarea name="remarks" class="ckeditor form-control">{{$bookingData->remarks}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label>Additional Nights:</label>
                                    <select class="custom-select form-control-border" name="additionalNights">
                                        <option value="">Select Value</option>
                                        <option value="1" <?php if($bookingData->additionalNights == "1") echo "selected"; ?> >1</option>
                                        <option value="2" <?php if($bookingData->additionalNights == "2") echo "selected"; ?> >2</option>
                                        <option value="3" <?php if($bookingData->additionalNights == "3") echo "selected"; ?> >3</option>
                                        <option value="4" <?php if($bookingData->additionalNights == "4") echo "selected"; ?> >4</option>
                                        <option value="5" <?php if($bookingData->additionalNights == "5") echo "selected"; ?> >5</option>
                                        <option value="6" <?php if($bookingData->additionalNights == "6") echo "selected"; ?> >6</option>
                                        <option value="7" <?php if($bookingData->additionalNights == "7") echo "selected"; ?> >7</option>
                                        <option value="8" <?php if($bookingData->additionalNights == "8") echo "selected"; ?> >8</option>
                                        <option value="9" <?php if($bookingData->additionalNights == "9") echo "selected"; ?> >9</option>
                                        <option value="10" <?php if($bookingData->additionalNights == "10") echo "selected"; ?> >10</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Additional Room:</label>
                                    <select class="custom-select form-control-border" name="additionalRooms">
                                        <option value="">Select Value</option>
                                        <option value="1" <?php if($bookingData->additionalRooms == "1") echo "selected"; ?> >1</option>
                                        <option value="2" <?php if($bookingData->additionalRooms == "2") echo "selected"; ?> >2</option>
                                        <option value="3" <?php if($bookingData->additionalRooms == "3") echo "selected"; ?> >3</option>
                                        <option value="4" <?php if($bookingData->additionalRooms == "4") echo "selected"; ?> >4</option>
                                        <option value="5" <?php if($bookingData->additionalRooms == "5") echo "selected"; ?> >5</option>
                                        <option value="6" <?php if($bookingData->additionalRooms == "6") echo "selected"; ?> >6</option>
                                        <option value="7" <?php if($bookingData->additionalRooms == "7") echo "selected"; ?> >7</option>
                                        <option value="8" <?php if($bookingData->additionalRooms == "8") echo "selected"; ?> >8</option>
                                        <option value="9" <?php if($bookingData->additionalRooms == "9") echo "selected"; ?> >9</option>
                                        <option value="10" <?php if($bookingData->additionalRooms == "10") echo "selected"; ?> >10</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Hotel Upgrade:</label>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="hotelUpgrade" value="true">
                                        <label class="form-check-label">Yes</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="hotelUpgrade" value="false" checked="">
                                        <label class="form-check-label">No</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Room Upgrade:</label>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="roomUpgrade" value="true">
                                        <label class="form-check-label">Yes</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="roomUpgrade" value="false" checked="">
                                        <label class="form-check-label">No</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Special Requests:</label>
                                    <textarea name="specialRequest" class="ckeditor form-control">{{$bookingData->others}}</textarea>
                                </div>
                                <!-- /.card-body -->
                                <input type="hidden" value="PUT" name="_method">
                                <input type="submit" value="Submit" class="btn btn-primary"> 
                            {!! Form::close() !!}
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <div class="col">
                    <div class="card card-primary sticky" style="width:30%">
                        <div class="card-header">
                            <h3 class="card-title">Holder Details</h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label>Voucher Holder:</label>
                                <p>{{$bookingData->voucherHolder}}</p>
                            </div>
                            <div class="form-group">
                                <label>Voucher No.</label>
                                <p>{{$bookingData->voucherNo}}</p>
                            </div>
                            <div class="form-group">
                                <label>Chosen Outlet/Destination:</label>
                                <p>{{$bookingData->chosenOutlet}}</p>
                            </div>
                            <div class="form-group">
                                <label>Status:</label>
                                <p>{{$bookingData->status}}</p>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
</div>
@endsection