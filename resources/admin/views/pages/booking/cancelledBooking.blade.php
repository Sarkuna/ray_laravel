@extends('layouts.dashboard')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Cancelled</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
                        <li class="breadcrumb-item"><a href="/booking/Pending">Pending</a></li>
                        <li class="breadcrumb-item active">Booking Cancelled</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <section class="content"><!-- Body -->
    <div class="container-fluid">
    <hr>
        <!-- /.card-header -->
            <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row"><div class="col-sm-12 col-md-6">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <table id="example1" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
                    <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Voucher No.: activate to sort column descending">Voucher No.</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Voucher Holder: activate to sort column ascending">Voucher Holder</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Booking Date: activate to sort column ascending">Booking Date</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Type: activate to sort column ascending">Type</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending">Status</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Amount: activate to sort column ascending">Amount</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Due Date: activate to sort column ascending">Due Date</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Action: activate to sort column ascending">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($bookingData as $booking)
                        @if($booking->status == "n/a" || $booking->status == "offline")
                        <tr role="row" class="odd">
                            <td class="dtr-control sorting_1" tabindex="0">
                                <!--Modal-->
                                 <a href="#" data-toggle="modal" data-target="#myModal" data-p_name="{{$booking->voucherNo}}">
                                    {{$booking->voucherNo}}
                                </a>
                                
                                <!--The Modal Container-->
                                <div class="modal fade" id="myModal">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <!--Modal Header-->
                                            <div class="modal-header">
                                            <h4 class="modal-title">Booking Details for {{$booking->voucherNo}}</h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <!--Modal body-->
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <div class="section">
                                                    <b>Name 1: </b>{{$booking->nameOne}}<br>
                                                    <b>Name 2: </b>{{$booking->nameTwo}}<br>
                                                    <b>Address: </b>{{$booking->address}}<br>
                                                    <b>Email: </b>{{$booking->email}}<br>
                                                    <b>Contact: </b>{{$booking->contactNo}}<br>
                                                    <b>Date 1: </b>{{$booking->dateOne}}<br>
                                                    <b>Date 2: </b>{{$booking->dateTwo}}<br>
                                                    <b>Date 3: </b>{{$booking->dateThree}}<br>
                                                    <b>Selected Offer: </b>{{$booking->selectedOffer}}<br>
                                                    <b>Choosen Outlet/Destination: </b>{{$booking->chosenOutlet}}
                                                    </div>
                                                </div>
                                            </div>
                                            <!--Modal footer-->
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--End of Modal-->
                            </td>
                            <td>{{$booking->voucherHolder}}</td>
                            <td>{{$booking->bookingDate}}</td>
                            <td>{{$booking->paymentType}}</td>
                            <td>{{$booking->status}}</td>
                            <td>RM {{$booking->paymentAmount}}</td>
                            <td>{{$booking->dueDate}}</td>
                            <td>
                                <div class="row">
                                    <button class="btn btn-success">Confirm</button><br>&nbsp;
                                    <a href="/booking/{{$booking->id}}/edit" class="btn btn-primary">Edit</a>&nbsp;
                                    {!!Form::open(['action' => ['BookingController@destroy', $booking->id], 'method' => 'POST', 'class' => 'item-btn'])!!}
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input class="btn btn-danger" type="submit" value="Delete">
                                    {!!Form::close()!!}
                                </div>
                                
                            </td>
                        </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.card-body --> 
    </div>
    </section>
</div>
@endsection