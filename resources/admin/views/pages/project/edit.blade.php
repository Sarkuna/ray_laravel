@extends('layouts.dashboard')  
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Project</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
                        <li class="breadcrumb-item"><a href="/project/main">Project</a></li>
                        <li class="breadcrumb-item active">Edit Project</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <section class="content">
        <div class="container-fluid">
            <!-- general form elements -->
            <div class="row">
                <div class="col-sm">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Edit Project</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            {!! Form::open(['action' => ['ProjectController@update', $projectData->id], 'method' => 'POST', 'files' => true]) !!}
                                <div class="form-group">
                                    <label>Client Name:</label>
                                    <select class="custom-select form-control-border" name="clientName">
                                        <option disabled value>-- Please select a Client --</option>
                                        @foreach ($clientData as $client)
                                            <option value="{{$client->clientName}}" <?php if($projectData->clientName == "$client->clientName") echo "selected"; ?>>{{$client->clientName}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Project Name (Reference):</label>
                                    <input type="text" name="projectName" class="form-control form-control-border border-width-2" value="{{$projectData->projectName}}">
                                </div>
                                <div class="form-group">
                                    <label>Project Prefix:</label>
                                    <input type="text" name="projectPrefix" class="form-control form-control-border border-width-2" value="{{$projectData->projectPrefix}}">
                                </div>
                                <div class="form-group">
                                    <label>Title(Optional):</label>
                                    <input type="text" name="title" class="form-control form-control-border border-width-2" value="{{$projectData->title}}">
                                </div>
                                <div class="form-group">
                                    <label>Greeting Note:</label>
                                    <textarea name="greetingNote" class="form-control">{{$projectData->greetingNote}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label>Greeting Banner(Optional):</label>
                                    @if ($projectData->greetingBanner == null)
                                        <p>No greeting banner was uploaded</p>
                                    @else
                                        <p>Current greeting banner:</p><img src="/images/projectBanner/{{$projectData->greetingBanner}}"><br><br>
                                    @endif
                                    <input type="file" name="greetingBanner" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Client Logo(Optional):</label>
                                    @if ($projectData->clientLogo == null)
                                        <p>No client logo was uploaded</p>
                                    @else
                                        <p>Current client logo:</p><img src="/images/clientLogo/{{$projectData->clientLogo}}"><br><br>
                                    @endif
                                <input type="file" name="clientLogo" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Expiry Date:</label>
                                    <input type="date" name="expiryDate" class="form-control" value="{{$projectData->expiryDate}}">
                                </div>
                                <div class="form-group">
                                    <label>Offers:</label>
                                    <select class="custom-select form-control-border" name="offers[]">
                                        <option disabled selected value>-- Please select an Offer --</option>
                                        @foreach ($offerData as $offer)
                                            <option value="{{$offer->referenceName}}" <?php if($projectData->offers == "$offer->referenceName") echo "selected"; ?>>{{$offer->referenceName}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <!-- /.card-body -->
                                <input type="hidden" value="PUT" name="_method">
                                <input type="submit" value="Submit" class="btn btn-primary"> 
                            {!! Form::close() !!}
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <div class="col">
                    @if($errors->any())
                        <div class="card card-danger sticky" style="width:40%">
                            <div class="card-header">
                                <h3 class="card-title">Errors</h3>
                            </div>
                            <div class="card-body">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
                    
        </div>
    </section>
</div>
@endsection