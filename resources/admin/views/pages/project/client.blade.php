@extends('layouts.dashboard')  
@section('content')


<div class="content-wrapper">
@include('inc.messages')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Client</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
                        <li class="breadcrumb-item"><a href="/project/main">Project</a></li>
                        <li class="breadcrumb-item active">Client</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <section class="content">
        <div class="container-fluid">
        <hr>
        <!-- /.card-header -->
            <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <a href="/client/create" class="btn btn-primary">Create Client</a>
                    </div>
                </div>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <table id="example1" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
                        <thead>
                            <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="No.: activate to sort column descending" style="width:5%">No.</th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Client Name: activate to sort column ascending">Client Name</th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Client Type: activate to sort column ascending">Client Type</th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Action: activate to sort column ascending">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($clientData as $client)
                                <tr role="row" class="odd">
                                    <td class="dtr-control sorting_1" tabindex="0">{{$client->id}}</td>
                                    <td>{{$client->clientName}}</td>
                                    <td>{{$client->clientType}}</td>
                                    <td>
                                        <div class="row">
                                            <a href="/client/{{$client->id}}/edit" class="btn btn-primary">Edit</a>&nbsp;
                                            {!!Form::open(['action' => ['ClientController@destroy', $client->id], 'method' => 'POST', 'class' => 'item-btn'])!!}
                                                <input name="_method" type="hidden" value="DELETE">
                                                <input class="btn btn-danger" type="submit" value="Delete">
                                            {!!Form::close()!!}
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection