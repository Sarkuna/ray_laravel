<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Login') }}</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('themes/adminlte3/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="{{ asset('themes/adminlte3/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
    <!-- icheck -->
    <link rel="stylesheet" href="{{ asset('themes/adminlte3/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <!-- JQVMap -->
    <link rel="stylesheet" href="{{ asset('themes/adminlte3/plugins/jqvmap/jqvmap.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('themes/adminlte3/dist/css/adminlte.min.css') }}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ asset('themes/adminlte3/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{ asset('themes/adminlte3/plugins/daterangepicker/daterangepicker.css') }}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('themes/adminlte3/plugins/summernote/summernote-bs4.min.css') }}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('themes/adminlte3/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/adminlte3/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/adminlte3/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <!-- CustomCSS -->
    <link rel="stylesheet" href="{{ asset('themes/adminlte3/custom/css/custom.css') }}">
</head>


<body class="hold-transition sidebar-mini layout-fixed">
  <div id="app">
    <div class="wrapper">
      <!-- Navbar -->
      @include('inc.navbar')
      <!-- /.navbar -->

      <!-- Main Sidebar Container -->
      @include('inc.sidebar')
      <!-- / Main Sidebar -->

      @yield('content')
      
      <footer class="main-footer">
          <strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io/themes/v3/index3.html">AdminLTE.io</a>.</strong>
          All rights reserved.
          <div class="float-right d-none d-sm-inline-block">
              <b>Version</b> 3.0.5
          </div>
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
      </aside>
      <!-- /.control-sidebar -->
    </div>
  </div>

<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{ asset('themes/adminlte3/plugins/jquery/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('themes/adminlte3/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{ asset('themes/adminlte3/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- Fontawesome JS -->
<script src="{{ asset('themes/adminlte3/plugins/fontawesome-free/js/all.js') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('themes/adminlte3/plugins/chart.js/Chart.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('themes/adminlte3/plugins/sparklines/sparkline.js') }}"></script>
<!-- JQVMap -->
<script src="{{ asset('themes/adminlte3/plugins/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('themes/adminlte3/plugins/jqvmap/maps/jquery.vmap.usa.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('themes/adminlte3/plugins/jquery-knob/jquery.knob.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('themes/adminlte3/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('themes/adminlte3/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('themes/adminlte3/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script> file is missing
<!-- Summernote -->
<script src="{{ asset('themes/adminlte3/plugins/summernote/summernote-bs4.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('themes/adminlte3/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('themes/adminlte3/dist/js/adminlte.min.js') }}"></script>
<!-- DataTables & Plugins -->
<script src="{{ asset('themes/adminlte3/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('themes/adminlte3/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('themes/adminlte3/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('themes/adminlte3/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('themes/adminlte3/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('themes/adminlte3/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('themes/adminlte3/plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('themes/adminlte3/plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('themes/adminlte3/plugins/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('themes/adminlte3/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('themes/adminlte3/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('themes/adminlte3/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
<!-- Custom Form Input -->
<script src="{{ asset('js/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
<!-- CustomeJS -->
<script src="{{ asset('themes/adminlte3/custom/js/repeater.js') }}"></script>
<!-- Ckeditor JS -->
<script src="{{ asset('themes/adminlte3/plugins/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('themes/adminlte3/plugins/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>

<script>
  $(document).ready(function (){
      $('#myModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
      })
  })
  $(document).ready(function (){
      $('#receiptModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
      })
  })
  $(document).ready(function (){
      $('#emailModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
      })
  })
  $(document).ready(function (){
      $('#statusModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
      })
  })
  $(document).ready(function (){
      $('#refundModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
      })
  })
  $(document).ready(function (){
      $('#invoiceModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
      })
  })
  $(document).ready(function (){
      $('#processingModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
      })
  }) 
  $(document).ready(function (){
      $('#paymentModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
      })
  })
  $(document).ready(function (){
      $('#invoiceModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
      })
  })
  $(document).ready(function (){
      $('#deleteConfirmModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
      })
  })
</script>
<script>
    $(document).ready(function () {
        $('.ckeditor').ckeditor();
    });
</script>

<script>
$(function () {
    $("#example1").DataTable({
      "responsive": true, 
      "lengthChange": false, 
      "autoWidth": false,
      "search": {
        "caseInsensitive": false,
      },
      "buttons": ["copy", "csv", "excel", "pdf", "print"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
 
<script>
  // function dateOneSelected(){
  //   var x = document.getElementById("date1");
  //   var dateTwoMinLimit = date('Y-m-d', strtotime("+1 day", strtotime(x)));
  //   var dateTwoMaxLimit = date('Y-m-d', strtotime("+2 week", strtotime(x)));

  //   document.getElementById("date2").min = dateTwoMinLimit;
  //   document.getElementById("date2").max = dateTwoMaxLimit;

  //   var dateThreeMinLimit = date('Y-m-d', strtotime("+1 day", strtotime($dateTwoMaxLimit)));
  //   var dateThreeMaxLimit = date('Y-m-d', strtotime("+2 week", strtotime($dateTwoMaxLimit)));

  //   document.getElementById("date3").min = dateThreeMinLimit;
  //   document.getElementById("date3").max = dateThreeMaxLimit;
    
  // }
</script>
</body>
</html>