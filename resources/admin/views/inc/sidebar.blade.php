

<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="index3.html" class="brand-link">
    <img src="{{ asset('images/banners/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
          style="opacity: .8">
    <span class="brand-text font-weight-light">Rewards and You</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{ asset('images/banners/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block">Alexander Pierce</a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
            with font-awesome or any other icon font library -->
        <li class="nav-header">NAVIGATIONS</li>
        <li class="nav-item menu-open">
          <a href="/dashboard" class="nav-link active">
            <i class="nav-icon fas fa-house-user"></i>
            <p>Admin Panel <?php ?></p>
          </a>
        </li>

        <!-- project module -->
        <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-project-diagram"></i>
            <p>Projects<i class="right fas fa-angle-left"></i></p>
          </a>
          <ul class="nav-item nav-treeview">
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Manage Offers<i class="right fas fa-angle-left"></i></p>
              </a>
              <ul class="nav-item nav-treeview">
                <li class="nav-item">
                  <a href="/project/offer/active" class="nav-link">
                    <i class="far fa-dot-circle nav-icon"></i>
                    <p>Active Offers</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="/project/offer/inactive" class="nav-link">
                    <i class="far fa-dot-circle nav-icon"></i>
                    <p>Inactive Offers</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="/project/offer/expired" class="nav-link">
                    <i class="far fa-dot-circle nav-icon"></i>
                    <p>Expired Offers</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item">
              <a href="/project/group" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Manage Groups</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="/project/client" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Manage Clients</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="/project/main" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Manage Projects</p>
              </a>
            </li>
          </ul>
        </li>
        
        <!-- voucher module -->
        <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-ticket-alt"></i>
            <p>Manage Vouchers<i class="right fas fa-angle-left"></i></p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/voucher/generate" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Generate</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="/voucher/extendVoucher" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Extend</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="/voucher/assignVoucher" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Assign</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="/voucher" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>View/Edit Vouchers</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="/voucher/viewActivation" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>View/Edit Activaitons</p>
              </a>
            </li>
          </ul>
        </li>

        <!-- booking module -->
        <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-suitcase"></i>
            <p>Manage Bookings<i class="fas fa-angle-left right"></i></p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/excel/newBooking" class="nav-link">
                <i class="fas fa-circle nav-icon"></i>
                <p>New Bookings</p>
                  <span class="badge badge-info right">
                    0
                  </span>
              </a>
            </li>
            <li class="nav-item">
              <a href="/excel/processBooking" class="nav-link">
                <i class="fas fa-circle nav-icon"></i>
                <p>Process Bookings</p>
                <span class="badge badge-info right">
                  0
                </span>
              </a>
            </li>
            <li class="nav-item">
              <a href="/excel/pendingPaymentBooking" class="nav-link">
                <i class="fas fa-circle nav-icon"></i>
                <p>Pending Payments</p>
                <span class="badge badge-info right">
                  0
                </span>
              </a>
            </li>
            <li class="nav-item">
              <a href="/excel/pendingReplyBooking" class="nav-link">
                <i class="fas fa-circle nav-icon"></i>
                <p>Pending Reply</p>
                <span class="badge badge-info right">
                  0
                </span>
              </a>
            </li>
            <li class="nav-item">
              <a href="/excel/unresponsiveBooking" class="nav-link">
                <i class="fas fa-circle nav-icon"></i>
                <p>Unresponsive</p>
                <span class="badge badge-info right">
                  0
                </span>
              </a>
            </li>
            <li class="nav-item">
              <a href="/excel/refundBooking" class="nav-link">
                <i class="fas fa-circle nav-icon"></i>
                <p>Refund</p>
                <span class="badge badge-info right">
                  0
                </span>
              </a>
            </li>
            <li class="nav-item">
              <a href="/excel/allBooking" class="nav-link">
                <i class="fas fa-circle nav-icon"></i>
                <p>All Bookings</p>
              </a>
            </li>
          </ul>
        </li>

        <!-- banner module -->
        <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-flag"></i>
            <p>Banners<i class="fas fa-angle-left right"></i></p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/banner/main" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Banner</p>
              </a>
            </li>
          </ul>
        </li>

        <!-- reports module -->       
        <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-scroll"></i>
            <p>Reports<i class="fas fa-angle-left right"></i></p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/report/logs" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Generate Reports</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="/report/statistics" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Customer Statistics</p>
              </a>
            </li>
          </ul>
        </li>

        <!-- admin logs module -->
        <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-cogs"></i>
            <p>Admin tools</p><i class="fas fa-angle-left right"></i></p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/log" class="nav-link">
                <i class="fas fa-circle nav-icon"></i>
                <p>Admin Logs</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="/setting" class="nav-link">
                <i class="fas fa-circle nav-icon"></i>
                <p>Admin Settings</p>
              </a>
            </li>
          </ul>
        </li>
       
        <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
            <i class="fas fa-hotel"></i>
            <p>HOTEL MODULE</p><i class="fas fa-angle-left right"></i>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/hotel/invoiceRequest" class="nav-link">
                <i class="fas fa-circle nav-icon"></i>
                <p>Invoice Request</p>
                <span class="badge badge-info right">
                  0
                </span>
              </a>
            </li>
            <li class="nav-item">
              <a href="/hotel/pendingInvoice" class="nav-link">
                <i class="fas fa-circle nav-icon"></i>
                <p>Pending Invoice</p>
                  <span class="badge badge-info right">
                    0
                  </span>
              </a>
            </li>
            <li class="nav-item">
              <a href="/hotel/invoiceDetail" class="nav-link">
                <i class="fas fa-circle nav-icon"></i>
                <p>Invoice Details</p>
                  <span class="badge badge-info right">
                    0
                  </span>
              </a>
            </li>
            <li class="nav-item">
              <a href="/hotel/paymentVoucher" class="nav-link">
                <i class="fas fa-circle nav-icon"></i>
                <p>Payment Voucher</p>
                  <span class="badge badge-info right">
                    0
                  </span>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
          <i class="fas fa-money-bill"></i>
            <p>PAYMENT</p><i class="fas fa-angle-left right"></i>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/payment/refundPayment" class="nav-link">
                <i class="fas fa-circle nav-icon"></i>
                <p>Refund Payment</p>
                <span class="badge badge-info right">
                  0
                </span>
              </a>
            </li>
            <li class="nav-item">
              <a href="/payment/hotelPayment" class="nav-link">
                <i class="fas fa-circle nav-icon"></i>
                <p>Hotel Payment</p>
                <span class="badge badge-info right">
                  0
                </span>
              </a>
            </li>
            <li class="nav-item">
              <a href="/payment/paymentReceipt" class="nav-link">
                <i class="fas fa-circle nav-icon"></i>
                <p>Payment Receipt</p>
                <span class="badge badge-info right">
                  0
                </span>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
          <i class="fas fa-atlas"></i>           
          <p>HISTORY</p><i class="fas fa-angle-left right"></i>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/history/completed" class="nav-link">
                <i class="fas fa-circle nav-icon"></i>
                <p>Completed</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="/history/cancelled" class="nav-link">
                <i class="fas fa-circle nav-icon"></i>
                <p>Cancelled</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
          <i class="fas fa-folder"></i>           
          <p>Templates</p><i class="fas fa-angle-left right"></i>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/template/hotel" class="nav-link">
                <i class="fas fa-circle nav-icon"></i>
                <p>Hotel Template</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="/template/email" class="nav-link">
              <i class="fas fa-circle nav-icon"></i>
                <p>Email Templates</p>
              </a>
            </li>
          </ul>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>