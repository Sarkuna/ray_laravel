<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>


    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{ asset('themes/travelfresh/css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/travelfresh/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/travelfresh/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/travelfresh/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/travelfresh/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/travelfresh/css/superfish.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/travelfresh/css/slicknav.min.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/travelfresh/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/travelfresh/css/spacing.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/travelfresh/css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/travelfresh/css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/travelfresh/css/chosen.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/travelfresh/css/datatable.min.css') }}">
</head>
<body>
    <div class="top-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-8">
                    <div class="top-header-left">
                        <p><i class="fa fa-phone"></i>954-648-1802</p>
                        <p><i class="fa fa-envelope-o"></i>info@yourwebsite.com</p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-4">
                    <div class="top-header-right">
                        <a href="#"><i class="fa fa-user-plus"></i>Registration</a>
                        <a href="#"><i class="fa fa-user-circle"></i>Login</a>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="menu-area">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-12">
                    <div class="logo">
                        <a href="#"><img src="https://envato.phpscriptpoint.com/travelfresh/public/uploads/logo.png" alt="Logo"></a>
                    </div>
                </div>
                <div class="col-md-9 col-sm-12">
                    <div class="nav-wrapper main-menu">
                        <nav>
                            <ul class="sf-menu" id="menu">
                                <li><a href="#">Home</a></li>
                                <li><a href="#">Services</a></li>
                                <li><a href="#">Destinations</a></li>
                                <li class="menu-item-has-children"><a href="javascript:void;">Pages</a>
                                    <ul>
                                        <li><a href="#">About Us</a></li>
                                        <li><a href="#">Our Team</a></li>
                                        <li><a href="#">Testimonial</a></li>
                                        <li><a href="#">FAQ</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">News</a></li>
                                <li><a href="#">Contact</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    @yield('content')

    
    

    <div class="footer-area pt_50 pb_80">
        <div class="container wow fadeIn">
            <div class="row">
                <div class="col-md-3 col-sm-6 wow fadeInLeft" data-wow-delay="0.2s">
                    <div class="footer-item mt_30">
                        <h3>Upcoming Tours</h3>
                        <ul>
                            <li><a href="https://envato.phpscriptpoint.com/travelfresh/package/view/36">15 days in Salina Island</a></li>
                            <li><a href="https://envato.phpscriptpoint.com/travelfresh/package/view/35">12 days in Salina Island</a></li>
                            <li><a href="https://envato.phpscriptpoint.com/travelfresh/package/view/34">7 days in Salina Island</a></li>
                            <li><a href="https://envato.phpscriptpoint.com/travelfresh/package/view/33">3 days in Salina Island</a></li>
                            <li><a href="https://envato.phpscriptpoint.com/travelfresh/package/view/31">7 days in Marrakesh</a></li>
                            <li><a href="https://envato.phpscriptpoint.com/travelfresh/package/view/30">12 days in Marrakesh</a></li>
                            <li><a href="https://envato.phpscriptpoint.com/travelfresh/package/view/29">15 days in Marrakesh</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 wow fadeIn" data-wow-delay="0.3s">
                    <div class="footer-item mt_30">
                        <h3>Featured Tours</h3>
                        <ul>
                            <li><a href="https://envato.phpscriptpoint.com/travelfresh/package/view/38">30 days in Salina Island</a></li>
                            <li><a href="https://envato.phpscriptpoint.com/travelfresh/package/view/35">12 days in Salina Island</a></li>
                            <li><a href="https://envato.phpscriptpoint.com/travelfresh/package/view/33">3 days in Salina Island</a></li>
                            <li><a href="https://envato.phpscriptpoint.com/travelfresh/package/view/31">7 days in Marrakesh</a></li>
                            <li><a href="https://envato.phpscriptpoint.com/travelfresh/package/view/29">15 days in Marrakesh</a></li>
                            <li><a href="https://envato.phpscriptpoint.com/travelfresh/package/view/27">30 days in Marrakesh</a></li>
                            <li><a href="https://envato.phpscriptpoint.com/travelfresh/package/view/26">3 days in Los Cabos</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 wow fadeIn" data-wow-delay="0.4s">
                    <div class="footer-item mt_30">
                        <h3>Recent News</h3>
                        <ul>
                            <li><a href="https://envato.phpscriptpoint.com/travelfresh/news/view/4">Ei qui possim abhorreant ei eam iudico disputando</a></li>
                            <li><a href="https://envato.phpscriptpoint.com/travelfresh/news/view/3">Est ei unum illum mucius, nemore alterum corpora at ius</a></li>
                            <li><a href="https://envato.phpscriptpoint.com/travelfresh/news/view/2">An usu natum aperiri accommodare, hendrerit tincidunt</a></li>
                            <li><a href="https://envato.phpscriptpoint.com/travelfresh/news/view/1">Vis nostro nominati electram ex aeterno voluptatum</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 wow fadeInRight" data-wow-delay="0.2s">
                    <div class="footer-item mt_30">
                        <h3>Address</h3>
                        <div class="footer-address-item">
                            <div class="icon"><i class="fa fa-map-marker"></i></div>
                            <div class="text"><span>3153 Foley Street<br />
                                    Miami, FL 33176</span></div>
                        </div>
                        <div class="footer-address-item">
                            <div class="icon"><i class="fa fa-phone"></i></div>
                            <div class="text"><span>Sales: 954-648-1802<br />
                                    Support: 963-612-1782</span></div>
                        </div>
                        <div class="footer-address-item">
                            <div class="icon"><i class="fa fa-envelope-o"></i></div>
                            <div class="text"><span>sales@yourwebsite.com<br />
                                    support@yourwebsite.com</span></div>
                        </div>
                        <ul class="footer-social">
                            <li><a href="http://www.facebook.com"><i class="fa fa-facebook"></i></a></li><li><a href="http://www.twitter.com"><i class="fa fa-twitter"></i></a></li><li><a href="http://www.linkedin.com"><i class="fa fa-linkedin"></i></a></li><li><a href="http://www.pinterest.com"><i class="fa fa-pinterest"></i></a></li><li><a href="https://www.youtube.com"><i class="fa fa-youtube"></i></a></li>                    </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="footer-bottom">
        <div class="container wow fadeIn">
            <div class="row">
                <div class="col-md-6">
                    <div class="copy-text">
                        <p>Copyright © 2020, TravelFresh. All Rights Reserved.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="footer-bottom-menu">
                        <ul>
                            <li><a href="https://envato.phpscriptpoint.com/travelfresh/term">Terms and Conditions</a></li>
                            <li><a href="https://envato.phpscriptpoint.com/travelfresh/privacy">Privacy Policy</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="scroll-top">
        <i class="fa fa-angle-up"></i>
    </div>


    <!-- All JS Files -->
    <script src="{{ asset('themes/travelfresh/js/jquery-2.2.4.min.js') }}"></script>
    <script src="{{ asset('themes/travelfresh/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('themes/travelfresh/js/chosen.jquery.js') }}"></script>
    <script src="{{ asset('themes/travelfresh/js/docsupport/init.js') }}"></script>
    <script src="{{ asset('themes/travelfresh/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('themes/travelfresh/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('themes/travelfresh/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('themes/travelfresh/js/easing.min.js') }}"></script>
    <script src="{{ asset('themes/travelfresh/js/wow.min.js') }}"></script>
    <script src="{{ asset('themes/travelfresh/js/superfish.js') }}"></script>
    <script src="{{ asset('themes/travelfresh/js/jquery.slicknav.min.js') }}"></script>
    <script src="{{ asset('themes/travelfresh/js/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('themes/travelfresh/js/viewportchecker.js') }}"></script>
    <script src="{{ asset('themes/travelfresh/js/waypoints.min.js') }}"></script>
    <script src="https://js.stripe.com/v2/"></script>
    <script src="{{ asset('themes/travelfresh/js/custom.js') }}"></script>



    <script>
        $(document).on('submit', '#stripe_form', function () {
            $('#submit-button').prop("disabled", true);
            $("#msg-container").hide();
            Stripe.card.createToken({
                number: $('.card-number').val(),
                cvc: $('.card-cvc').val(),
                exp_month: $('.card-expiry-month').val(),
                exp_year: $('.card-expiry-year').val()
                // name: $('.card-holder-name').val()
            }, stripeResponseHandler);
            return false;
        });
        Stripe.setPublishableKey('pk_test_0SwMWadgu8DwmEcPdUPRsZ7b');
        function stripeResponseHandler(status, response) {
            if (response.error) {
                $('#submit-button').prop("disabled", false);
                $("#msg-container").html('<div style="color: red;border: 1px solid;margin: 10px 0px;padding: 5px;"><strong>Error:</strong> ' + response.error.message + '</div>');
                $("#msg-container").show();
            } else {
                var form$ = $("#stripe_form");
                var token = response['id'];
                form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
                form$.get(0).submit();
            }
        }
    </script>

    <script>
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "3000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
    </script>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'article-ckeditor' );
    </script>
</body>
</html>
