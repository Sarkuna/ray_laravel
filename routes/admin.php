<?php

use App\Http\Middleware\AuthCheck; //the middleware for checking login status

Auth::routes();
Route::get('/login', 'Admin\AuthController@index');
Route::get('/', 'Admin\AuthController@index');

Route::get('/registration', 'Admin\AuthController@registration');

Route::post('post-login', 'Admin\AuthController@postLogin')->name('admin.login.post'); 
Route::post('post-registration', 'Admin\AuthController@postRegistration'); 
Route::get('/logout', 'Admin\AuthController@logout')->name('admin.logout');


Route::group(['middleware' => [AuthCheck::class]], function () {
    //add routes that require user to be logged in for access
    Route::get('/dashboard', 'Admin\AuthController@dashboard');
    Route::get('/booking/pending', 'PagesController@bookingMain');
    Route::get('/booking/completed','PagesController@bookingCompleted');
    Route::get('/booking/cancelled','PagesController@bookingCancelled');

    Route::get('/project/offer','PagesController@projectOffer');
    Route::get('/project/group','PagesController@projectGroups');
    Route::get('/project/client','PagesController@projectClients');
    Route::get('/project/main','PagesController@project');

    Route::get('/voucher/generate','PagesController@generateVoucher');
    Route::get('/voucher/extendVoucher','PagesController@extendVoucher');
    Route::get('/voucher/assignVoucher','PagesController@assignVoucher');
    Route::get('/voucher/viewActivation','PagesController@viewActivation');
    Route::get('/voucher/main','PagesController@voucher');

    Route::get('/banner/main','PagesController@bannerMain');

    Route::get('/report/statistics','PagesController@customerStats');
    Route::get('/report','PagesController@reports');

    Route::get('/log','PagesController@adminLogs');
    Route::get('/setting','PagesController@adminSetting');

    Route::resource('offer', 'OfferController');
    Route::resource('client', 'ClientController');
    Route::resource('project', 'ProjectController');
    Route::resource('voucher', 'VoucherController');
    Route::resource('group', 'GroupController');
    Route::resource('banner', 'BannerController');
    Route::resource('booking', 'BookingController');
    Route::resource('adminSetting', 'AdminController');

    // new changes
    Route::get('/project/offer/active','PagesController@offerActive');
    Route::get('/project/offer/inactive','PagesController@offerInactive');
    Route::get('/project/offer/expired','PagesController@offerExpired');

    Route::post('/booking/main/setSuccessful', 'DatabaseController@bookingSuccessful');

    // Excel
    Route::get('/excel/allBooking', 'PagesController@allBookings');
    Route::get('/excel/newBooking', 'PagesController@newBookings');
    Route::post('/excel/newBooking/moveBooking', 'DatabaseController@confirmingBooking');
    Route::post('/excel/newBooking/receiptUpload', 'DatabaseController@uploadReceipt');
    Route::post('/excel/newBooking/bookingEmail', 'DatabaseController@sendEmail');

    Route::get('/excel/processBooking', 'PagesController@processBookings');
    Route::post('/excel/processBooking/invoice', 'DatabaseController@invoiceStore');

    Route::get('/excel/pendingBooking', 'PagesController@pendingBookings');
    Route::post('/excel/pendingBooking/setStatus', 'DatabaseController@setStatus');
    Route::post('/excel/pendingBooking/refund', 'DatabaseController@refundBooking');
    Route::post('/excel/pendingBooking/receipt', 'DatabaseController@uploadReceipt');

    Route::get('/excel/pendingPaymentBooking', 'PagesController@pendingPayment');
    Route::get('/excel/pendingReplyBooking', 'PagesController@pendingReply');
    Route::get('/excel/refundBooking', 'PagesController@refundBooking');
    Route::get('/excel/unresponsiveBooking', 'PagesController@unresponsiveBooking');

    Route::get('/hotel/invoiceRequest', 'PagesController@invoiceRequestBooking');
    Route::post('/hotel/invoiceEmail', 'DatabaseController@invoiceEmail');
    Route::get('/hotel/pendingInvoice', 'PagesController@pendingInvoice');
    Route::post('/hotel/refEmail', 'DatabaseController@refEmail');
    Route::get('/hotel/invoiceDetail', 'PagesController@invoiceDetail');
    Route::post('/hotel/submitInvoiceDetail', 'DatabaseController@submitInvoiceDetail');
    Route::get('/hotel/paymentVoucher', 'PagesController@paymentVoucher');

    Route::get('/payment/refundPayment', 'PagesController@refundPayment');
    Route::post('/payment/refundPayment', 'DatabaseController@refundPayment');
    Route::get('/payment/hotelPayment', 'PagesController@hotelPayment');
    Route::post('/payment/hotelPayment', 'DatabaseController@hotelPayment');
    Route::get('/payment/paymentReceipt', 'PagesController@paymentReceipt');
    Route::post('/payment/uploadPaymentReceipt', 'DatabaseController@uploadPaymentReceipt');

    Route::get('/history/completed', 'PagesController@completedBooking');
    Route::get('/history/cancelled', 'PagesController@cancelledBooking');
    Route::post('/history/restore', 'DatabaseController@restoreBooking');

    Route::get('/template/email', 'PagesController@emailTemplate');
    Route::get('/template/hotel', 'PagesController@hotelTemplate');

    Route::resource('template', 'TemplateController');
    Route::resource('hotelTemplate', 'HotelController');
});