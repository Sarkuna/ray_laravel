<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class offer extends Model
{
    protected $table = 'offer';

    protected $casts = [
        'locations' => 'array',
    ];
}
