<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Offer;
use DB, Auth, Image;

class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bookingFormQuery = DB::table('booking_form')->select('type')->get();
        $groupQuery = DB::table('group')->get();
        $data = array(
            'bookingFormData' => $bookingFormQuery,
            'groupData' => $groupQuery,
        );
        return view('pages.offer.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'referenceName' => 'required',
            'offerName' => 'required',
            'description' => 'required',
            'status' => 'required',
            'termsAndCondition' => 'required',
            'bookingForm' => 'required',
            'groupType' => 'required',
            'expiryDate' => 'required',
            'bookingRate' => 'required',
            'locations' => 'required',
        ]);

        $offer = new Offer;

        $offer->referenceName = $request->input('referenceName');
        $offer->offerName = $request->input('offerName');
        $offer->description = $request->input('description');
        $offer->termsConditions = $request->input('termsAndCondition');
        $offer->bookingForm = $request->input('bookingForm');
        $offer->groupType = $request->input('groupType');
        $offer->expiryDate = $request->input('expiryDate');
        $offer->payment = $request->input('bookingPayment');
        $offer->bookingRate = $request->input('bookingRate');
        $offer->locations = $request->input('locations');
        $offer->created_by = Auth::user()->email;
        $offer->last_edited_by = Auth::user()->email;

        $accountLevel = Auth::user()->acctype;

        //image storage
        $thumbnail = $request->file('thumbnail');
        if($thumbnail != null){
            $filename = time().'.'.$thumbnail->getClientOriginalExtension();
            Image::make($thumbnail)->save(public_path('/images/offerThumbnail/'.$filename));
            $offer->thumbnail = $filename;
        }

        $banner = $request->file('banner');
        if($banner != null) {
            $filename = time().'.'.$banner->getClientOriginalExtension();
            Image::make($banner)->save(public_path('/images/offerBanner/'.$filename));
            $offer->banner = $filename;
        }
        //image storage
        
        $date = date("d-m-Y H:i:s");

        $adminLogOfferCreate = DB::statement("INSERT INTO admin_log (email, acctype, action, created_at) 
        VALUES ('$offer->created_by', '$accountLevel', 'Created Offer: $offer->offerName, Reference Name: $offer->referenceName', '$date')");

        $offer->save(); //must be the after DB::statement line or else it will duplicate

        return redirect('/project/offer/active')->with('success', "Succesfully created offer: $offer->offerName");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $offer = offer::find($id);
        $bookingFormQuery = DB::table('booking_form')->select('type')->get();
        $groupQuery = DB::table('group')->get();

        $data = array(
            'offerData' => $offer,
            'bookingFormData' => $bookingFormQuery,
            'groupData' => $groupQuery,
        );

        return view('pages.offer.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'referenceName' => 'required',
            'offerName' => 'required',
            'description' => 'required',
            'termsAndCondition' => 'required',
            'bookingForm' => 'required',
            'groupType' => 'required',
            'bookingRate' => 'required',
            'locations' => 'required',
        ]);

        $offer = Offer::find($id);

        $offer->referenceName = $request->input('referenceName');
        $offer->offerName = $request->input('offerName');
        $offer->description = $request->input('description');
        $offer->termsConditions = $request->input('termsAndCondition');
        $offer->bookingForm = $request->input('bookingForm');
        $offer->payment = $request->input('bookingPayment');
        $offer->bookingRate = $request->input('bookingRate');
        $offer->locations = $request->input('locations');

        $offer->created_by = Auth::user()->email;
        $offer->last_edited_by = Auth::user()->email;
        $accountLevel = Auth::user()->acctype;

        //image storage
        $thumbnail = $request->file('thumbnail');
        if($thumbnail != null){
            $filename = time().'.'.$thumbnail->getClientOriginalExtension();
            Image::make($thumbnail)->save(public_path('/images/offerThumbnail/'.$filename));
            $offer->thumbnail = $filename;
        }

        $banner = $request->file('banner');
        if($banner != null) {
            $filename = time().'.'.$banner->getClientOriginalExtension();
            Image::make($banner)->save(public_path('/images/offerBanner/'.$filename));
            $offer->banner = $filename;
        }
        //image storage
        
        $date = date("d-m-Y H:i:s");

        $adminLogOfferCreate = DB::statement("INSERT INTO admin_log (email, acctype, action, created_at) 
        VALUES ('$offer->created_by', '$accountLevel', 'Edited Offer: $offer->offerName, Reference Name: $offer->referenceName', '$date')");

        $offer->save(); //must be the after DB::statement line or else it will duplicate

        return redirect('/project/offer')->with('success', "Succesfully edited offer: $offer->offerName");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $offer = offer::find($id);

        $accountEmail = Auth::user()->email;
        $accountLevel = Auth::user()->acctype;

        $date = date("d-m-Y H:i:s");

        $adminLogOfferDelete = DB::statement("INSERT INTO admin_log (email, acctype, action, created_at) 
        VALUES ('$accountEmail', '$accountLevel', 'Deleted Offer: $offer->offerName, Reference Name: $offer->referenceName', '$date')");

        $offer->delete();
        return redirect('/project/offer')->with('danger', "Succesfully deleted offer: $offer->offerName");
    }
}
