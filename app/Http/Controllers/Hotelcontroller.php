<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hotel;
use DB, Auth;

class Hotelcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.template.hotelCreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'hotelName' => 'required',
            'hotelBank' => 'required',
            'hotelBankAcc' => 'required',
            'picName' => 'required',
            'picEmail' => 'required',
            'hotelContact' => 'required',
        ]);
        $hotel = new Hotel;

        $hotel->hotelName = $request->input('hotelName');
        $hotel->hotelBank = $request->input('hotelBank');
        $hotel->hotelBankAcc = $request->input('hotelBankAcc');
        $hotel->picName = $request->input('picName');
        $hotel->picEmail = $request->input('picEmail');
        $hotel->hotelContact = $request->input('hotelContact');
       
        $hotel->created_by = Auth::user()->email;
        $hotel->last_edited_by = Auth::user()->email;
        $accountLevel = Auth::user()->acctype;

        $date = date("d-m-Y H:i:s");
        $adminLogHotelTemplateCreate = DB::statement("INSERT INTO admin_log (email, acctype, action, created_at) 
        VALUES ('$hotel->created_by', '$accountLevel', 'Created Template: $hotel->hotelName', '$date')");
        $hotel->save();

        return redirect('/template/hotel')->with('success', "Succesfully created template: $hotel->hotelName");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $hotel = Hotel::find($id);

        $data = array(
            'hotelData' => $hotel,
        );

        return view('pages.template.hotelEdit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'hotelName' => 'required',
            'hotelBank' => 'required',
            'hotelBankAcc' => 'required',
            'picName' => 'required',
            'picEmail' => 'required',
            'hotelContact' => 'required',
        ]);
        $hotel = Hotel::find($id);

        $hotel->hotelName = $request->input('hotelName');
        $hotel->hotelBank = $request->input('hotelBank');
        $hotel->hotelBankAcc = $request->input('hotelBankAcc');
        $hotel->picName = $request->input('picName');
        $hotel->picEmail = $request->input('picEmail');
        $hotel->hotelContact = $request->input('hotelContact');
       
        $hotel->created_by = Auth::user()->email;
        $hotel->last_edited_by = Auth::user()->email;
        $accountLevel = Auth::user()->acctype;

        $date = date("d-m-Y H:i:s");
        $adminLogHotelTemplateEdit = DB::statement("INSERT INTO admin_log (email, acctype, action, created_at) 
        VALUES ('$hotel->created_by', '$accountLevel', 'Edited Hotel: $hotel->hotelName', '$date')");
        $hotel->save();

        return redirect('/template/hotel')->with('success', "Succesfully created template: $hotel->hotelName");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}
