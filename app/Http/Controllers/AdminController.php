<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB, Auth, Image, Hash;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'accType' => 'required',
            'status' => 'required',
            'email' => 'required',
            'password' => 'required',
            // 'permissions' => 'required',
        ]);

        $user = new User;

        $user->name = $request->input('username');
        $user->acctype = $request->input('accType');
        $user->status = $request->input('status');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        // $user->permissions = $request->input('permissions');
        $user->created_by = Auth::user()->email;
        $user->last_edited_by = Auth::user()->email;

        $accountLevel = Auth::user()->acctype;
        
        $date = date("Y-m-d H:i:s");

        $adminLogAdminCreate = DB::statement("INSERT INTO admin_log (email, acctype, action, created_at) 
        VALUES ('$user->created_by', '$accountLevel', 'Created Admin: $user->name', '$date')");

        $user->save(); //must be the after DB::statement line or else it will duplicate

        return redirect('/setting')->with('success', "Succesfully created admin: $user->name");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return ($request);

        $this->validate($request, [
            'username' => 'required',
            'accType' => 'required',
            'status' => 'required',
            // 'email' => 'required',
            // 'password' => 'required',
            // 'permissions' => 'required',
        ]);

        $user = User::find($id);

        $user->name = $request->input('username');
        $user->acctype = $request->input('accType');
        $user->status = $request->input('status');
        // $user->email = $request->input('email');
        // $user->permissions = $request->input('permissions');
        $user->last_edited_by = Auth::user()->email;

        if ($request->input('password') != null){
            $user->password = $request->input('password');
        }

        $accountLevel = Auth::user()->acctype;
        
        $date = date("Y-m-d H:i:s");

        $adminLogAdminEdit = DB::statement("INSERT INTO admin_log (email, acctype, action, created_at) 
        VALUES ('$user->email', '$accountLevel', 'Edited Admin: $user->name', '$date')");

        $user->save(); //must be the after DB::statement line or else it will duplicate

        return redirect('/setting')->with('success', "Succesfully edited admin: $user->name");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = user::find($id);

        $accountEmail = Auth::user()->email;
        $accountLevel = Auth::user()->acctype;

        $date = date("Y-m-d H:i:s");

        $adminLogAdminDeleted = DB::statement("INSERT INTO admin_log (email, acctype, action, created_at) 
        VALUES ('$user->email', '$accountLevel', 'Deleted Admin: $user->name', '$date')");

        $user->delete();
        return redirect('/project/offer')->with('danger', "Succesfully deleted offer: $offer->offerName");
    }
}
