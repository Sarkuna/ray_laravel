<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Template;
use DB, Auth;

class TemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.template.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'templateName' => 'required',
            'templateContent' => 'required',
        ]);
        $template = new Template;

        $template->templateName = $request->input('templateName');
        $template->templateDescription = $request->input('templateDescription');
        $template->hotelEmail = $request->input('hotelEmail');
        $template->templateContent = $request->input('templateContent');
       
        $template->created_by = Auth::user()->email;
        $template->last_edited_by = Auth::user()->email;
        $accountLevel = Auth::user()->acctype;

        $date = date("d-m-Y H:i:s");

        $adminLogTemplateCreate = DB::statement("INSERT INTO admin_log (email, acctype, action, created_at) 
        VALUES ('$template->created_by', '$accountLevel', 'Created Template: $template->templateName', '$date')");

        $template->save();

        return redirect('/template/main')->with('success', "Succesfully created template: $template->templateName");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $template = Template::find($id);

        $data = array(
            'templateData' => $template,
        );

        return view('pages.template.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'templateName' => 'required',
            'templateContent' => 'required',
        ]);

        $template = Template::find($id);

        $template->templateName = $request->input('templateName');
        $template->templateDescription = $request->input('templateDescription');
        $template->hotelEmail = $request->input('hotelEmail');
        $template->templateContent = $request->input('templateContent');
       
        $template->created_by = Auth::user()->email;
        $template->last_edited_by = Auth::user()->email;
        $accountLevel = Auth::user()->acctype;

        $date = date("d-m-Y H:i:s");

        $adminLogTemplateEdit = DB::statement("INSERT INTO admin_log (email, acctype, action, created_at) 
        VALUES ('$template->created_by', '$accountLevel', 'Edited: $template->templateName', '$date')");

        $template->save();

        return redirect('/template/main')->with('success', "Succesfully edited template: $template->templateName");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $template = Template::find($id);
        $template->created_by = Auth::user()->email;
        $accountLevel = Auth::user()->acctype;

        $date = date("d-m-Y H:i:s");

        $adminLogTemplateDelete = DB::statement("INSERT INTO admin_log (email, acctype, action, created_at) 
        VALUES ('$template->created_by', '$accountLevel', 'Deleted: $template->templateName', '$date')");

        $template->delete();
        return redirect('/template/main')->with('success', "Succesfully deleted template: $template->templateName");
    }
}
