<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Booking;
use DB, Auth;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bookingQuery = Booking::find($id);
        $offerQuery = DB::table('offer')->get();
        $voucherQuery = DB::table('voucher')->where('voucherNo', "$bookingQuery->voucherNo")->get();

        // return ($voucherQuery); die;
        $data = array(
            'bookingData' => $bookingQuery,
            'offerData' => $offerQuery,
            'voucherData' => $voucherQuery,
        );
        return view('pages.booking.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            //not sure what to validate, cant access the booking creation form
        ]);

        $booking = Booking::find($id);

        $booking->nameOne = $request->input('nameOne');
        $booking->nameTwo = $request->input('nameTwo');
        $booking->address = $request->input('address');
        $booking->email = $request->input('email');
        $booking->contactNo = $request->input('contactNo');
        $booking->dateOne = $request->input('dateOne');
        $booking->dateTwo = $request->input('dateTwo');
        $booking->dateThree = $request->input('dateThree');
        $booking->confirmedOutlet = $request->input('confirmedOutlet');
        $booking->status = $request->input('status');
        $booking->statusNote = $request->input('statusNote');
        $booking->dueDate = $request->input('dueDate');
        $booking->paymentAmount = $request->input('paymentAmount');
        $booking->confirmedDate = $request->input('confirmedDate');
        $booking->remarks = $request->input('remarks');
        $booking->additionalNights = $request->input('additionalNights');
        $booking->additionalRooms = $request->input('additionalRooms');
        $booking->hotelUpgrade = $request->input('hotelUpgrade');
        $booking->roomUpgrade = $request->input('roomUpgrade');
        $booking->specialRequest = $request->input('specialRequest');
        $booking->last_edited_by = Auth::user()->email;

        $accountLevel = Auth::user()->acctype;

        $date = date("d-m-Y H:i:s");

        $adminLogBookingEdit = DB::statement("INSERT INTO admin_log (email, acctype, action, created_at) 
        VALUES ('$booking->last_edited_by', '$accountLevel', 'Edited Booking: $booking->voucherNo', '$date')");

        $booking->save();

        return redirect('/booking/pending')->with('success', "Succesfully edited booking ID: $booking->id");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $booking = Booking::find($id);
        $accountEmail = Auth::user()->email;
        $accountLevel = Auth::user()->acctype;

        $date = date("d-m-Y H:i:s");

        $adminLogBookingDelete = DB::statement("INSERT INTO admin_log (email, acctype, action, created_at) 
        VALUES ('$accountEmail', '$accountLevel', 'Deleted Booking: $booking->voucherNo', '$date')");

        $booking->status = "CANCELLED";
        $booking->save();
        return redirect()->back()->with('danger', "Succesfully deleted booking: $booking->id");
    }
}
