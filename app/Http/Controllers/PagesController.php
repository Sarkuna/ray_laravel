<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB, Validator, Auth;

class PagesController extends Controller
{
    public function testPage(){
        $query = DB::table('voucher')->get();
        $data = array(
            'voucherData' => $query,
        );

        return view('pages.test', $data);
    }
    
    #project module
    public function project(){
        $query = DB::table('project')->get();
        $data = array(
            'projectsData' => $query,
        );
        return view('pages.project.projectMain', $data);

    }
    public function projectOffer(){
        $query = DB::table('offer')->get();
        $data = array(
            'offerData' => $query,
        );
        return view('pages.project.offer', $data); 
    }
    public function projectGroups(){
        $query = DB::table('group')->get();
        $data = array(
            'groupData' => $query,
        );
        return view('pages.project.group', $data);
    }
    public function projectClients(){
        $query = DB::table('client')->get();
        $data = array(
            'clientData' => $query,
        );
        return view('pages.project.client', $data);
    }

    public function bookingMain(){
        $query = DB::table('booking')->where('status', '=', 'pending')->get();
        $data = array(
            'bookingData' => $query,
        );
        return view('pages.booking.bookingMain', $data);
    }
    public function bookingCompleted(){
        $query = DB::table('booking')->where('status', '=', 'successful')->get();
        $data = array(
            'bookingData' => $query,
        );
        return view('pages.booking.completedBooking', $data);
    }
    public function bookingCancelled(){
        $query = DB::table('booking')->where('status', '=', 'N/A')->get();
        $data = array(
            'bookingData' => $query,
        );
        return view('pages.booking.cancelledBooking', $data);
    }

    public function voucher(){
        return view('pages.voucher.viewVoucher');
    }
    public function generateVoucher(){
        $clientQuery = DB::table('client')->get();
        $projectQuery = DB::table('project')->get();
        $voucherQuery = DB::table('voucher')->get();

        $data = array(
            'clientData' => $clientQuery,
            'projectData' => $projectQuery,
            'voucherData' => $voucherQuery,
        );

        return view('pages.voucher.generateVoucher', $data);
    }
    public function extendVoucher(){
        return view('pages.voucher.extendVoucher');
    }
    public function assignVoucher(){
        return view('pages.voucher.assignVoucher');
    }
    public function viewActivation(){
        return view('pages.voucher.viewActivation');
    }

    public function bannerMain(){
        $bannerQuery = DB::table('banner')->get();
        $data = array(
            'bannerData' => $bannerQuery,
        );
        return view('pages.banner.bannerMain', $data);
    }

    public function adminLogs(){
        $query = DB::table('admin_log')->get();
        $data = array(
            'logData' => $query,
        );
        return view('pages.adminLog', $data);
    }
    public function adminSetting(){
        $query = DB::table('users')->get();
        $data = array(
            'userData' => $query,
        );
        return view('pages.adminSetting.adminSettingMain', $data);
    }

    // New Changes

    public function offerActive(){
        $query = DB::table('offer')->where('status', '=', 'active')->get();
        $data = array(
            'offerData' => $query,
        );
        return view('pages.offer.offerActive', $data); 
    }
    public function offerInactive(){
        $query = DB::table('offer')->where('status', '=', 'inactive')->get();
        $data = array(
            'offerData' => $query,
        );
        return view('pages.offer.offerInactive', $data); 
    }
    public function offerExpired(){
        $query = DB::table('offer')->where('status', '=', 'expired')->get();
        $data = array(
            'offerData' => $query,
        );
        return view('pages.offer.offerExpired', $data); 
    }

    // Excel Part
    public function newBookings(){
        $query = DB::table('booking')->where('status', '=', 'NEW BOOKING')->get();
        $data = array(
            'bookingData' => $query,
        );
        return view('pages.excel.newBooking', $data);
    }
    public function pendingBookings(){
        $query = DB::table('booking')->where('status', '=', 'PENDING')->get();
        $data = array(
            'bookingData' => $query,
        );
        return view('pages.excel.pendingBooking', $data);
    }
    public function processBookings(){
        //booking to voucher to client
        $bookingQuery = DB::table('booking')->where('status', '=', 'processing')->get();
        $voucherQuery = DB::table('voucher')->get();
        $clientQuery = DB::table('client')->get();
        $data = array(
            'bookingData' => $bookingQuery,
            'voucherData' => $voucherQuery,
            'clientData' => $clientQuery,
        );
        return view('pages.excel.processBooking', $data);
    }
    public function pendingPayment(){
        $query = DB::table('booking')->where('status', '=', 'PENDING PAYMENT')->get();
        $data = array(
            'bookingData' => $query,
        );
        return view('pages.excel.pendingPayment', $data);
    }
    public function pendingReply(){
        $query = DB::table('booking')->where('status', '=', 'PENDING REPLY')->get();
        $data = array(
            'bookingData' => $query,
        );
        return view('pages.excel.pendingReply', $data);
    }
    public function refundBooking(){
        $query = DB::table('booking')->where('status', '=', 'REFUND')->get();
        $data = array(
            'bookingData' => $query,
        );
        return view('pages.excel.refundBooking', $data);
    }
    public function unresponsiveBooking(){
        $query = DB::table('booking')->where('status', '=', 'UNRESPONSIVE')->get();
        $data = array(
            'bookingData' => $query,
        );
        return view('pages.excel.unresponsiveBooking', $data);
    }
    public function allBookings(){
        $query = DB::table('booking')->get();
        $data = array(
            'bookingData' => $query,
        );
        return view('pages.excel.allBooking', $data);
    }

    public function emailTemplate(){
        $query = DB::table('template')->get();
        $data = array(
            'templateData' => $query,
        );
        return view('pages.template.templateMain', $data);
    }

    public function invoiceRequestBooking(){
        $query = DB::table('booking')->where('status', '=', 'INVOICE REQUEST')->get();
        $templateQuery = DB::table('template')->where('id', '=', '1')->get();
        $data = array(
            'bookingData' => $query,
            'templateData' => $templateQuery,
        );
        return view('pages.hotel.invoiceRequest', $data);
    }
    public function pendingInvoice(){
        $query = DB::table('booking')->where('status', '=', 'PENDING INVOICE')->get();
        $templateQuery = DB::table('template')->where('id', '=', '2')->get();
        $data = array(
            'bookingData' => $query,
            'templateData' => $templateQuery,
        );
        return view('pages.hotel.pendingInvoice', $data);
    }
    public function invoiceDetail(){
        $query = DB::table('booking')->where('status', '=', 'INVOICE DETAIL')->get();
        $templateQuery = DB::table('template')->where('id', '=', '3')->get();
        $data = array(
            'bookingData' => $query,
            'templateData' => $templateQuery,
        );
        return view('pages.hotel.invoiceDetail', $data);
    }
    public function paymentVoucher(){
        $query = DB::table('booking')->where('status', '=', 'PAYMENT VOUCHER')->get();
        $data = array(
            'bookingData' => $query,
        );
        return view('pages.hotel.paymentVoucher', $data);
    }
    public function refundPayment(){
        $query = DB::table('booking', 'invoice')
                    ->where('status', '=', 'REFUND PAYMENT')
                    ->orWhere('status', '=', 'REFUND')
                    ->join('invoice', 'booking.voucherNo', '=', 'invoice.voucherNo')
                    ->select(DB::raw('booking.*, invoice.*, booking.id as booking_id'))->get();
        $data = array(
            'jointData' => $query,
        );
        return view('pages.payment.refundPayment', $data);
    }
    public function hotelPayment(){
        $query = DB::table('booking', 'invoice')
                    ->where('status', '=', 'HOTEL PAYMENT')
                    ->join('invoice', 'booking.voucherNo', '=', 'invoice.voucherNo')
                    ->select(DB::raw('booking.*, invoice.*, booking.id as booking_id'))->get();
        $data = array(
            'jointData' => $query,
        );
        return view('pages.payment.hotelPayment', $data);
    }
    public function paymentReceipt(){
        $query = DB::table('booking', 'invoice')
                    ->where('status', '=', 'PAYMENT RECEIPT')
                    ->join('invoice', 'booking.voucherNo', '=', 'invoice.voucherNo')
                    ->select(DB::raw('booking.*, invoice.*, booking.id as booking_id'))->get();
        $data = array(
            'jointData' => $query,
        );
        return view('pages.payment.paymentReceipt', $data);
    }

    public function completedBooking(){
        $query = DB::table('booking', 'invoice')
                    ->where('status', '=', 'COMPLETED')
                    ->join('invoice', 'booking.voucherNo', '=', 'invoice.voucherNo')
                    ->select(DB::raw('booking.*, invoice.*, booking.id as booking_id'))->get();
        $data = array(
            'jointData' => $query,
        );
        return view('pages.payment.completePayment', $data);
    }
    public function cancelledBooking(){
        $query = DB::table('booking', 'invoice')
                    ->where('status', '=', 'CANCELLED')
                    ->join('invoice', 'booking.voucherNo', '=', 'invoice.voucherNo')
                    ->select(DB::raw('booking.*, invoice.*, booking.id as booking_id'))->get();
        $data = array(
            'jointData' => $query,
        );
        return view('pages.payment.cancelPayment', $data);
    }

    public function hotelTemplate(){
        $query = DB::table('hotel')->get();
        $data = array(
            'hotelData' => $query,
        );
        return view('pages.template.hotelMain', $data);
    }
}