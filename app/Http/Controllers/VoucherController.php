<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Voucher;
use DB, Auth;

class VoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'projectPrefix' => 'required',
            'clientName' => 'required',
            'totalVouchers' => 'required',
            'startingNumber' => 'required',
            'expiryDate' => 'required',
            'number' => 'required',
            'timeUnit' => 'required',
        ]);
        $count = 0;
        $permitted_chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        $prefix = $request->input('projectPrefix');
        $startingNumber = $request->input('startingNumber');
        $totalVoucher = $request->input('totalVouchers');

        $dateLimit = "-" . $request->input('number') . " " . $request->input('timeUnit');

        // DB::table('project')->select('projectName')->where('projectPrefix', '=', $prefix); 
        // $projectName = DB::statement("SELECT projectName FROM project WHERE projectPrefix = '$prefix'");
        while ($count <= $totalVoucher){
            $voucher = new Voucher;
            $voucherNo = str_pad($startingNumber,4,"0",STR_PAD_LEFT);

            $generatedVoucher = $prefix." ".$voucherNo;

            $voucher->voucherNo = $generatedVoucher;
            $voucher->activationCode = substr(str_shuffle($permitted_chars), 0, 5);
            $voucher->expiryDate = $request->input('expiryDate');
            $voucher->dateLimiter = $dateLimit;
            $voucher->clientName = $request->input('clientName');
            // $voucher->projectName = $projectName;
            $voucher->created_by = Auth::user()->email;
            $voucher->last_edited_by = Auth::user()->email;
            $accountLevel = Auth::user()->acctype;
            $date = date("d-m-Y H:i:s");

            $count = $count + 1;
            $startingNumber = $startingNumber + 1;

            $voucher->save();
        }
        $adminLogVoucherCreate = DB::statement("INSERT INTO admin_log (email, acctype, action, created_at) 
        VALUES ('$voucher->created_by', '$accountLevel', 'Created Vouchers: Prefix = $prefix, Amount = $totalVoucher', '$date')");

        return redirect("/voucher/generate")->with('success', "Succesfully created vouchers");

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
