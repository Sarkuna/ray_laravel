<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Group;
use App\Offer;
use DB, Auth;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.group.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'groupName' => 'required',
        ]);
        $group = new Group;
        $group->groupName = $request->input('groupName');
        $group->created_by = Auth::user()->email;
        $group->last_edited_by = Auth::user()->email;
        $accountLevel = Auth::user()->acctype;

        $date = date("d-m-Y H:i:s");

        $adminLogGroupCreate = DB::statement("INSERT INTO admin_log (email, acctype, action, created_at) 
        VALUES ('$group->created_by', '$accountLevel', 'Created group $group->groupName (group)', '$date')");

        $group->save(); //must be the after DB::statement line or else it will duplicate

        return redirect('/project/group')->with('success', "Succesfully created group: $group->groupName");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $groupQuery = Group::find($id);
        $offerQuery = DB::table('offer')->get();

        $data = array(
            'offerData' => $offerQuery,
            'groupData' => $groupQuery,
        );

        return view('pages.group.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'groupName' => 'required',
        ]);

        $group = Group::find($id);

        $group->groupName = $request->input('groupName');
        $group->created_by = Auth::user()->email;
        $group->last_edited_by = Auth::user()->email;

        $accountLevel = Auth::user()->acctype;

        $date = date("d-m-Y H:i:s");

        $adminLogGroupEdit = DB::statement("INSERT INTO admin_log (email, acctype, action, created_at) 
        VALUES ('$group->created_by', '$accountLevel', 'Edited Group: $group->groupName', '$date')");

        $group->save(); //must be the after DB::statement line or else it will duplicate

        return redirect('/project/group')->with('success', "Succesfully edited group: $group->groupName");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group = Group::find($id);

        $accountEmail = Auth::user()->email;
        $accountLevel = Auth::user()->acctype;

        $date = date("d-m-Y H:i:s");

        $adminLogOfferDelete = DB::statement("INSERT INTO admin_log (email, acctype, action, created_at) 
        VALUES ('$accountEmail', '$accountLevel', 'Deleted Group: $group->groupName', '$date')");

        $group->delete();
        return redirect('/project/group')->with('danger', "Succesfully deleted group: $group->groupName");
    }
}
