<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Banner;
use DB, Auth, Image;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.banner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'showIn' => 'required',
            'alignment' => 'required',
            'bannerNote' => 'required',
        ]);
        $banner = new Banner;
        $banner->showIn = $request->input('showIn');
        $banner->bannerLink = $request->input('bannerLink');
        $banner->alignment = $request->input('alignment');
        $banner->bannerNote = $request->input('bannerNote');
        $banner->created_by = Auth::user()->email;
        $banner->last_edited_by = Auth::user()->email;
        $accountLevel = Auth::user()->acctype;
        //banner storage
        switch($banner->alignment) {
            case "topBanner":
                $bannerData = $request->file('bannerImage');
                $filename = time().'.'.$bannerData->getClientOriginalExtension();
                Image::make($bannerData)->save(public_path("/images/banners/$banner->alignment/".$filename));
                $banner->bannerImage = $filename;
                $successText = 'Top Banner';
                break;
            case "homepageBanner":
                $bannerData = $request->file('bannerImage');
                $filename = time().'.'.$bannerData->getClientOriginalExtension();
                Image::make($bannerData)->save(public_path("/images/banners/$banner->alignment/".$filename));
                $banner->bannerImage = $filename;
                $successText = 'Homepage Banner';
                break;
            case "leftBanner":
                $bannerData = $request->file('bannerImage');
                $filename = time().'.'.$bannerData->getClientOriginalExtension();
                Image::make($bannerData)->save(public_path("/images/banners/$banner->alignment/".$filename));
                $banner->bannerImage = $filename;
                $successText = 'Left Banner';
                break;
            case "rightBanner":
                $bannerData = $request->file('bannerImage');
                $filename = time().'.'.$bannerData->getClientOriginalExtension();
                Image::make($bannerData)->save(public_path("/images/banners/$banner->alignment/".$filename));
                $banner->bannerImage = $filename;
                $successText = 'Right Banner';
                break;
            case "bottomBanner":
                $bannerData = $request->file('bannerImage');
                $filename = time().'.'.$bannerData->getClientOriginalExtension();
                Image::make($bannerData)->save(public_path("/images/banners/$banner->alignment/".$filename));
                $banner->bannerImage = $filename;
                $successText = 'Bottom Banner';
                break;
        } //banner storage logic
        
        $date = date("d-m-Y H:i:s");

        $adminLogBannerCreate = DB::statement("INSERT INTO admin_log (email, acctype, action, created_at) 
        VALUES ('$banner->created_by', '$accountLevel', 'Created Banner: $filename, with alignment $banner->alignment', '$date')");

        $banner->save(); //must be the after DB::statement line or else it will duplicate

        return redirect("/banner/main")->with('success', "Succesfully created banner: $banner->id");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bannerQuery = Banner::find($id);
        $data = array(
            'bannerData' => $bannerQuery,
        );
        return view('pages.banner.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'showIn' => 'required',
            'alignment' => 'required',
        ]);

        $banner = Banner::find($id);

        $banner->showIn = $request->input('showIn');
        $banner->bannerLink = $request->input('bannerLink');
        $banner->alignment = $request->input('alignment');
        $banner->bannerNote = $request->input('bannerNote');
        $banner->created_by = Auth::user()->email;
        $banner->last_edited_by = Auth::user()->email;

        $accountLevel = Auth::user()->acctype;

        $bannerFile = $request->file('bannerImage');
        
        if($bannerFile != null){
            //banner storage
            switch($banner->alignment) {
                case "topBanner":
                    $bannerData = $request->file('bannerImage');
                    $filename = time().'.'.$bannerData->getClientOriginalExtension();
                    Image::make($bannerData)->save(public_path("/images/banners/$banner->alignment/".$filename));
                    $banner->bannerImage = $filename;
                    break;
                case "homepageBanner":
                    $bannerData = $request->file('bannerImage');
                    $filename = time().'.'.$bannerData->getClientOriginalExtension();
                    Image::make($bannerData)->save(public_path("/images/banners/$banner->alignment/".$filename));
                    $banner->bannerImage = $filename;
                    break;
                case "leftBanner":
                    $bannerData = $request->file('bannerImage');
                    $filename = time().'.'.$bannerData->getClientOriginalExtension();
                    Image::make($bannerData)->save(public_path("/images/banners/$banner->alignment/".$filename));
                    $banner->bannerImage = $filename;
                    break;
                case "rightBanner":
                    $bannerData = $request->file('bannerImage');
                    $filename = time().'.'.$bannerData->getClientOriginalExtension();
                    Image::make($bannerData)->save(public_path("/images/banners/$banner->alignment/".$filename));
                    $banner->bannerImage = $filename;
                    break;
                case "bottomBanner":
                    $bannerData = $request->file('bannerImage');
                    $filename = time().'.'.$bannerData->getClientOriginalExtension();
                    Image::make($bannerData)->save(public_path("/images/banners/$banner->alignment/".$filename));
                    $banner->bannerImage = $filename;
                    break;
            } //banner storage logic
        }
        
        $date = date("d-m-Y H:i:s");

        $adminLogBannerEdit = DB::statement("INSERT INTO admin_log (email, acctype, action, created_at) 
        VALUES ('$banner->created_by', '$accountLevel', 'Edited Banner: $banner->bannerImage, with alignment: $banner->alignment', '$date')");

        $banner->save(); //must be the after DB::statement line or else it will duplicate

        return redirect("/banner/main")->with('success', "Succesfully edited banner ID: $banner->id");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner = banner::find($id);
        $accountEmail = Auth::user()->email;
        $accountLevel = Auth::user()->acctype;

        $date = date("d-m-Y H:i:s");

        $adminLogBannerDelete = DB::statement("INSERT INTO admin_log (email, acctype, action, created_at) 
        VALUES ('$banner->created_by', '$accountLevel', 'Deleted Banner: $banner->bannerImage (banner), with alignment: $banner->alignment (alignment)', '$date')");

        $banner->delete();
        return redirect("/banner/main")->with('success', "Succesfully deleted banner ID: $banner->id");
    }
}
