<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use App\Mail\BookingEmail;
use DB, Image, Auth;
use App\Booking;

use Illuminate\Http\Request;

class DatabaseController extends Controller
{
    public function bookingSuccessful(Request $request){
        $bookingId = $request->input('bookingId');
        $booking = Booking::find($bookingId);

        $booking->status = "SUCCESSFUL";

        $adminLogBookingEdit = DB::statement("INSERT INTO admin_log (email, acctype, action, created_at) 
        VALUES ('$booking->last_edited_by', '$accountLevel', 'Edited Booking: $booking->voucherNo', '$date')");

        $booking->save();
        return redirect('/booking/main')->with('success', "Succesfully edited booking ID: $booking->id");
    }

    public function confirmingBooking(Request $request){
        $this->validate($request, [
            'hotelName' => 'required',
            'checkInDate' => 'required',
            'location' => 'required',
        ]);
        $bookingId = $request->input('bookingId');
        $booking = Booking::find($bookingId);

        $booking->confirmedOutlet = $request->input('hotelName');
        $booking->confirmedDate = $request->input('checkInDate');
        $booking->confirmedLocation = $request->input('location');
        
        if($booking->receipt != null){
            $booking->status = "INVOICE REQUEST";
            $booking->save();
            return redirect()->back()->with('success', "Booking Details are confirmed with Receipt. Booking moved to Invoice Request");
        } else {
            $booking->status = "PENDING PAYMENT";
            $booking->save();
            return redirect()->back()->with('success', "Booking Details are confirmed with no Receipt. Booking moved to Pending Payment");
        }

    }

    public function sendEmail(Request $request){
        $this->validate($request, [
            'content' => 'required',
        ]);
        $bookingId = $request->input('bookingId');
        $booking = Booking::find($bookingId);
        
        $booking->status = "PROCESSING";
        $booking->emailSentDate = date("d-m-Y");

        Mail::send(new BookingEmail($request));
        $booking->save();
        return redirect('/excel/newBooking')->with('success', 'Email is sent, booking moved to PROCESSING');
    }

    public function uploadReceipt(Request $request){
        $bookingId = $request->input('bookingId');
        $booking = Booking::find($bookingId);

        $receiptData = $request->file('receiptImage');
        $filename = time().'.'.$receiptData->getClientOriginalExtension();
        Image::make($receiptData)->save(public_path("/images/receipt/".$filename));
        $booking->receipt = $filename;

        if($booking->confirmedOutlet != null){
            $booking->status = "INVOICE REQUEST";
            $booking->save();
            return redirect()->back()->with('success', "Uploaded receipt for Voucher No: $booking->voucherNo. Booking moved to Invoice Request");
        } else {
            $booking->save();
            return redirect()->back()->with('success', "Uploaded receipt for Voucher No: $booking->voucherNo");
        }
    }

    public function setStatus(Request $request){
        $this->validate($request, [
            'setStatus' => 'required',
        ]);
        $bookingId = $request->input('bookingId');
        $booking = Booking::find($bookingId);

        $booking->status = $request->input('setStatus');
        // $booking->save();
        return redirect()->back()->with('success', "Status set to $booking->status, for Voucher $booking->voucherNo");
    }

    public function refundBooking(Request $request){
        $bookingId = $request->input('bookingId');
        $booking = Booking::find($bookingId);

        $voucherNo = $booking->voucherNo;
        $bankName = $request->input('bankName');
        $accountNo = $request->input('accountNo');
        $email = $request->input('email');

        $refundQuery = DB::statement("INSERT INTO refund (voucherNo, bankName, accountNo, email)
        VALUES ('$voucherNo', '$bankName', '$accountNo', '$email')");

        $booking->paymentType = "REFUND PAYMENT";
        $booking->save();
        return redirect('/excel/allBooking')->with('success', "Refund submitted for booking ID: $booking->id");
    }
    public function invoiceEmail(Request $request){
        $bookingId = $request->input('bookingId');
        $booking = Booking::find($bookingId);
        $booking->status = "PENDING INVOICE";

        // $emailContent = $request->input('content');
        // $searchTerms = array('voucherHolder','checkInDate','confirmedOutlet','confirmedLocation');
        // $replacements = array($booking->voucherHolder, $booking->confirmedDate, $booking->confirmedOutlet, $booking->confirmedLocation);
        // $result = str_replace($searchTerms, $replacements, $emailContent);
        // Mail::send(new BookingEmail($result));

        // Mail::send(new BookingEmail($request));
        $booking->save();

        return redirect('/hotel/invoiceRequest')->with('success', "Invoice Request is sent, Booking is moved to Pending Invoice");
    }
    public function refEmail(Request $request){
        Mail::send(new BookingEmail($request));
        $bookingId = $request->input('bookingId');
        $booking = Booking::find($bookingId);
        $booking->status = "INVOICE DETAIL";
        $booking->save();

        $voucherNo = $request->input('voucherNo');
        $customerName = $request->input('voucherHolder');
        $checkInDate = $request->input('confirmedDate');
        $hotelName = $request->input('confirmedOutlet');
        $referenceNO = $request->input('refNo');

        $created_by = Auth::user()->email;
        $last_edited_by = Auth::user()->email;

        $invoiceRequestQuery = DB::statement("INSERT INTO invoice (voucherNo, hotelName, customerName, checkInDate, created_by, last_edited_by) 
        VALUES ('$voucherNo','$hotelName', '$customerName', '$checkInDate', '$created_by', '$last_edited_by')");

        return redirect('/hotel/pendingInvoice')->with('success', 'Invoice Details is saved, booking moved to Invoice Details');

    }
    public function emailKeyword(Request $request){
        $bookingId = $request->input('bookingId');
        $booking = Booking::find($bookingId);
        $emailContent = $request->input('content');

        $searchTerms = array('voucherHolder','checkInDate','confirmedOutlet','confirmedLocation');

        $replacements = array($booking->voucherHolder, $booking->confirmedDate, $booking->confirmedOutlet, $booking->confirmedLocation);

        Mail::send(new BookingEmail(str_replace($searchTerms, $replacements, $emailContent)));
    }

    public function submitInvoiceDetail(Request $request){
        $this->validate($request, [
            'confirmedOutlet' => 'required',
            'hotelBank' => 'required',
            'hotelBankAcc' => 'required',
            'voucherHolder' => 'required',
            'bookingRefNo' => 'required',
            'confirmedDate' => 'required',
            'amount' => 'required',
        ]);
        $bookingId = $request->input('bookingId');
        $booking = Booking::find($bookingId);
        $hotelName = $request->input('confirmedOutlet');
        $hotelBank = $request->input('hotelBank');
        $hotelBankAcc = $request->input('hotelBankAcc');
        $voucherHolder = $request->input('voucherHolder');
        $bookingRefNo = $request->input('bookingRefNo');
        $confirmedDate = $request->input('confirmedDate');
        $amount = $request->input('amount');

        $invoiceDetailInsert = DB::update("UPDATE invoice SET bookingRefNo = '$bookingRefNo', hotelName = '$hotelName', hotelBank = '$hotelBank', hotelBankAcc = '$hotelBankAcc', amount = '$amount', customerName = '$voucherHolder', checkInDate = '$confirmedDate' WHERE voucherNo = ?", [$request->input('voucherNo')]);

        $booking->status = "PAYMENT VOUCHER";
        $booking->save();
        return redirect('/hotel/invoiceDetail');
    }

    public function refundPayment(Request $request){
        $this->validate($request,[
            'confirmedOutlet' => 'required',
            'hotelBank' => 'required',
            'hotelBankAcc' => 'required',
            'voucherHolder' => 'required',
            'confirmedDate' => 'required',
            'bookingRate' => 'required',
        ]);
        $bookingId = $request->input('bookingId');
        $booking = Booking::find($bookingId);

        $booking->status = "PAYMENT RECEIPT";
        $booking->save();
        return redirect('/payment/refundPayment');

    }
    public function hotelReceipt(Request $request){
        $this->validate($request,[
            'confirmedOutlet' => 'required',
            'hotelBank' => 'required',
            'hotelBankAcc' => 'required',
            'voucherHolder' => 'required',
            'confirmedDate' => 'required',
            'bookingRate' => 'required',
        ]);
        $bookingId = $request->input('bookingId');
        $booking = Booking::find($bookingId);

        $booking->status = "PAYMENT RECEIPT";
        $booking->save();
        return redirect('/payment/hotelPayment');

    }
    public function uploadPaymentReceipt(Request $request){
        $bookingId = $request->input('bookingId');
        $booking = Booking::find($bookingId);
        
        $paymentReceiptImage = $request->file('paymentReceiptImage');
        $filename = time().'.'.$paymentReceiptImage->getClientOriginalExtension();
        Image::make($paymentReceiptImage)->save(public_path("/images/receipt/".$filename));

        $invoicePaymentReceipt = DB::update("UPDATE invoice SET paymentReceipt = '$filename' WHERE voucherNo = ?", [$request->input('voucherNo')]);
        // Mail::send(new BookingEmail($request));

        $booking->status = "COMPLETED";
        $booking->save();
        return redirect('/payment/paymentReceipt');
    }

    public function restoreBooking(Request $request){
        $bookingId = $request->input('bookingId');
        $booking = Booking::find($bookingId);

        $booking->status = "NEW BOOKING";
        $booking->save();
        return redirect('/history/completed')->with('success', 'Booking was restored, please go to New Booking to view it');
    }
}
