<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use DB, Auth;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.client.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'clientName' => 'required',
            'clientType' => 'required',
        ]);
        $client = new Client;
        $client->clientName = $request->input('clientName');
        $client->clientType = $request->input('clientType');
        $accountEmail = Auth::user()->email;
        $client->created_by = $accountEmail;
        $client->last_edited_by = Auth::user()->email;

        $accountLevel = Auth::user()->acctype;

        $date = date("d-m-Y H:i:s");

        $adminLogClientCreate = DB::statement("INSERT INTO admin_log (email, acctype, action, created_at) 
        VALUES ('$accountEmail', '$accountLevel', 'Created Client: $client->clientName, Type: $client->clientType', '$date')");

        $client->save();

        return redirect('/project/client')->with('success', "Succesfully created client: $client->clientName");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = client::find($id);

        return view('pages.client.edit')->with('client', $client);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'clientName' => 'required',
            'clientType' => 'required',
        ]);
        $client = Client::find($id);
        $client->clientName = $request->input('clientName');
        $client->clientType = $request->input('clientType');
        $client->last_edited_by = Auth::user()->email;
        $accountLevel = Auth::user()->acctype;

        $date = date("d-m-Y H:i:s");

        $adminLogClientEdit = DB::statement("INSERT INTO admin_log (email, acctype, action, created_at) 
        VALUES ('$client->last_edited_by', '$accountLevel', 'Edited Client: $client->clientName, Type: $client->clientType', '$date')");

        $client->save();
        return redirect('/project/client')->with('success', "Succesfully edited client: $client->clientName");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = client::find($id);
        $accountEmail = Auth::user()->email;
        $accountLevel = Auth::user()->acctype;

        $date = date("d-m-Y H:i:s");

        $adminLogClientDelete = DB::statement("INSERT INTO admin_log (email, acctype, action, created_at) 
        VALUES ('$accountEmail', '$accountLevel', 'Deleted Client: $client->clientName, Type: $client->clientType', '$date')");

        $client->delete();
        return redirect('/project/client')->with('success', "Succesfully deleted client: $client->clientName");;
    }
}
