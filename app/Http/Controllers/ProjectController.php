<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use DB, Auth, Image;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $offerQuery = DB::table('offer')->select('referenceName')->get();
        $clientQuery = DB::table('client')->select('clientName')->get();

        $data = array(
            'offerData' => $offerQuery,
            'clientData' => $clientQuery,
        );
        return view('pages.project.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'clientName' => 'required',
            'projectName' => 'required',
            'projectPrefix' => 'required',
            'greetingNote' => 'required',
            'expiryDate' => 'required',
            'offers' => 'required',
        ]);
        $project = new Project;

        $project->clientName        = $request->input('clientName');
        $project->projectName       = $request->input('projectName');
        $project->projectPrefix     = $request->input('projectPrefix');
        $project->title             = $request->input('title');
        $project->greetingNote      = $request->input('greetingNote');
        $project->greetingBanner    = $request->input('greetingBanner');
        $project->clientLogo        = $request->input('clientLogo');
        $project->expiryDate        = $request->input('expiryDate');
        $project->offers            = $request->input('offers');

        $project->created_by = Auth::user()->email;
        $project->last_edited_by = Auth::user()->email;
        $accountLevel = Auth::user()->acctype;

        //image storage
        $greetingBanner = $request->file('greetingBanner');
        if($greetingBanner != null){
            $filename = time().'.'.$greetingBanner->getClientOriginalExtension();
            Image::make($greetingBanner)->save(public_path('/images/projectBanner/'.$filename));
            $project->greetingBanner = $filename;
        }
        
        $clientLogo = $request->file('clientLogo');
        if($clientLogo != null){
            $filename = time().'.'.$clientLogo->getClientOriginalExtension();
            Image::make($clientLogo)->save(public_path('/images/clientLogo/'.$filename));
            $project->clientLogo = $filename; 
        }
        //image storage

        $date = date("d-m-Y H:i:s");

        $adminLogProjectCreate = DB::statement("INSERT INTO admin_log (email, acctype, action, created_at) 
        VALUES ('$project->created_by', '$accountLevel', 'Created Project: $project->projectName, Project Name: $project->clientName', '$date')");

        $project->save();

        return redirect('/project/main')->with('success', "Succesfully created project: $project->projectName");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = Project::find($id);
        $offerQuery = DB::table('offer')->select('referenceName')->get();
        $clientQuery = DB::table('client')->select('clientName')->get();

        $data = array(
            'offerData' => $offerQuery,
            'clientData' => $clientQuery,
            'projectData' => $project,
        );

        return view('pages.project.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'clientName' => 'required',
            'projectName' => 'required',
            'projectPrefix' => 'required',
            'greetingNote' => 'required',
            'expiryDate' => 'required',
            'offers' => 'required',
        ]);
        $project = Project::find($id);

        $project->clientName        = $request->input('clientName');
        $project->projectName       = $request->input('projectName');
        $project->projectPrefix     = $request->input('projectPrefix');
        $project->title             = $request->input('title');
        $project->greetingNote      = $request->input('greetingNote');
        $project->greetingBanner    = $request->input('greetingBanner');
        $project->clientLogo        = $request->input('clientLogo');
        $project->expiryDate        = $request->input('expiryDate');
        $project->offers            = $request->input('offers');

        $project->created_by = Auth::user()->email;
        $project->last_edited_by = Auth::user()->email;
        $accountLevel = Auth::user()->acctype;

        //image storage
        $greetingBanner = $request->file('greetingBanner');
        if($greetingBanner != null){
            $filename = time().'.'.$greetingBanner->getClientOriginalExtension();
            Image::make($greetingBanner)->save(public_path('/images/projectBanner/'.$filename));
            $project->greetingBanner = $filename;
        }
        
        $clientLogo = $request->file('clientLogo');
        if($clientLogo != null){
            $filename = time().'.'.$clientLogo->getClientOriginalExtension();
            Image::make($clientLogo)->save(public_path('/images/clientLogo/'.$filename));
            $project->clientLogo = $filename; 
        }
        //image storage

        $date = date("d-m-Y H:i:s");

        $adminLogProjectEdit = DB::statement("INSERT INTO admin_log (email, acctype, action, created_at) 
        VALUES ('$project->created_by', '$accountLevel', 'Edited Project: $project->projectName, Project Name: $project->clientName', '$date')");

        $project->save();

        return redirect('/project/main')->with('success', "Succesfully edited project: $project->projectName");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Project::find($id);
        $project->created_by = Auth::user()->email;
        $accountLevel = Auth::user()->acctype;

        $date = date("d-m-Y H:i:s");

        $adminLogProjectDelete = DB::statement("INSERT INTO admin_log (email, acctype, action, created_at) 
        VALUES ('$project->created_by', '$accountLevel', 'Deleted Project: $project->projectName, Project Name: $project->clientName', '$date')");


        $project->delete();
        return redirect('/project/main')->with('success', "Succesfully deleted project: $project->projectName");
    }
}
