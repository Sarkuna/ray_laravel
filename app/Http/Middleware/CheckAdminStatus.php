<?php

namespace App\Http\Middleware;

use Closure;

class CheckAdminStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $query = DB::statement('user')->where('email' == '$request->email')->get();
        if ($request->email == false){
            return redirect('/login')->with('error', 'Please login first');
        }
        return $next($request);
    }
}
