<?php

namespace App\Http\Middleware;

use Closure, Auth;

class AuthCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() == false){
            return redirect('/login')->with('error', 'Please login first');
        }
        return $next($request);
    }
}
