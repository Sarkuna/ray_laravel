<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Request $request)
    {
        Schema::defaultStringLength(191);
        $url = explode('.', $request->getHost(), 1);        
        $res = preg_replace('/^www\./', '', $url[0] );
        $subDomain = explode('.',  $res)[0]; 

        //$subDomain = 'rayadmin';
        /*$path_array = $request->segments();
        $admin_route = config('app.admin_route');

        if(in_array($admin_route, $path_array)) {
            config(['app.app_scope' == 'rayadmin']);
        }
        
        $app_scope = config('app.app_scope');*/


        if($subDomain == 'rayadmin') {
            $path = resource_path('admin/views');
        }else {
            $path = resource_path('front/views');
        }
        view()->addLocation($path);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
